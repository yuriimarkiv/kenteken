<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OnlineStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('photo')->nullable();
            $table->integer('price')->nullable();
            $table->timestamps();
        });

        Schema::create('buys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price');
            $table->integer('count');
            $table->integer('good_id')->unsigned()->index();
            $table->foreign('good_id')
                ->references('id')->on('goods')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('address')->nullable();
            $table->string('status')->nullable();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('order_buy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->integer('buy_id')->unsigned()->index();
            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');
            $table->foreign('buy_id')
                ->references('id')->on('buys')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_buy', function (Blueprint $table) {
            $table->dropForeign('order_buy_order_id_foreign');
            $table->dropForeign('order_buy_buy_id_foreign');
        });
        Schema::dropIfExists('order_buy');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_user_id_foreign');
        });
        Schema::dropIfExists('orders');

        Schema::table('buys', function (Blueprint $table) {
            $table->dropForeign('buys_good_id_foreign');
        });
        Schema::dropIfExists('buys');

        Schema::dropIfExists('goods');
    }
}
