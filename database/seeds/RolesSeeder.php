<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use App\User;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'admin']);
        Permission::create(['name' => 'user']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('admin');
        $role = Role::create(['name' => 'user']);
        $role->givePermissionTo('user');

        //creating admin
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('asdfasdf')
        ]);
        $user = User::find(1);
        $user->assignRole('admin');
    }
}
