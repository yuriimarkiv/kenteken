<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('home1');
//    //return view('welcome');
//});

Route::get('/', 'KentekenController@home');
Route::post('/kenteken/make-pdf', 'KentekenController@makePDF');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['permission:admin']], function () {
    Route::resource('users', 'UserController');
    Route::resource('goods', 'GoodController');
    Route::resource('orders', 'OrderController');

    Route::get('statistics/index', 'StatisticsController@index');
    Route::get('statistics/analytics', 'StatisticsController@analytics');
    Route::get('statistics/traffic', 'StatisticsController@traffic');
});

Route::get('/buy', 'BuyController@buy');
Route::get('/buy/cart', 'BuyController@cart');
Route::get('/buy/add-to-cart/{id}', 'BuyController@addToCart');
Route::post('/buy/confirm', 'BuyController@confirm');

Route::get('/pdf', 'KentekenController@pdf');

Route::get('/kenteken/buy-pdf', 'KentekenController@buyPDF');
Route::get('/kenteken/pay-pdf', 'KentekenController@payPDF');
Route::get('/kenteken/show-pdf', 'KentekenController@showPDF');
Route::get('/kenteken/cabinet', 'KentekenController@cabinet');

Route::post('/kenteken/pdfSubmit', 'KentekenController@pdfSubmit');
Route::post('/kenteken/submitBuyPDF', 'KentekenController@submitBuyPDF');
Route::post('/kenteken/submitPayPDF', 'KentekenController@submitPayPDF');
Route::post('/kenteken/show-short-pdf', 'KentekenController@showShortPDF');
Route::get('/kenteken/confirm-mollie', 'KentekenController@confirmMollie');