<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderBuy extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'buy_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function buy()
    {
        return $this->belongsTo('App\Buy');
    }

    protected $table = 'order_buy';
}
