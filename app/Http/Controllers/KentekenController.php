<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Illuminate\Support\Facades\Storage;
use App\Buypdf;
use App\User;
use Illuminate\Support\Facades\Hash;

class KentekenController extends Controller {
    public function home()
    {
        return view('home1');
    }

    public function pdf()
    {
        return view('kenteken/pdf');
    }

    public function makePDF(Request $request)
    {
        $carNumber = $request->input('kentekenInput');

        //constants
        $vweUser = 'Cell01';
        $vwePassword = 'nyx66M';
        $vweTypes = ['SB-ATL-MMT', 'SB-ATL-OPTIE-FABRIEK', 'SB-ATL-PP', 'SB-MILIEU-BASIC', 'SB-MRB', 'SB-OKR-CHECK', 'SB-RDW-ADVANCED', 'SB-RDW-HIST-ADV', 'SB-RDW-IMPORTLAND', 'SB-ATD-TCO', 'SB-ATD-TYRE-INFO', 'SB-ATL-TAX-ONLINE', 'SB-RDW-HIST-APK', 'SB-RDW-OVIZ-REAL', 'SB-RDW-STATUS-HIST', 'SB-RDW-ADVANCED', 'SB-STAT-WP-STA-BEZIT'];

        /*if (file_exists('storage/pdf/' . $carNumber . '.pdf')) {
            if (time() - filemtime("storage/pdf/$carNumber.pdf") < 14 * 24 * 60 * 60) {
                //echo "<a href=". "/storage/pdf/$carNumber.pdf" .">download</a>";
                return view('kenteken/viewPDF', [
                    'carNumber' => $carNumber
                ]);
            }
        } else {
            Storage::delete("public/pdf/$carNumber.pdf");
        }*/

        $postDataGenerated = [];
        foreach($vweTypes as $item) {
            $request =
                "requestXml=<bericht>
                    <authenticatie>
                        <naam>$vweUser</naam>
                        <wachtwoord>$vwePassword</wachtwoord>
                        <berichtsoort>$item</berichtsoort>
                        <referentie></referentie>
                    </authenticatie>
                    <parameters>
                        <kenteken>$carNumber</kenteken>
                    </parameters>
                    </bericht>";
            $postDataGenerated[$item] = $request;
        }


        global $parsed;
        $parsed = [];


        //receiving information
        $connectionHost = "acceptatie-interdata.vwe.nl";
        $connectionPath = "/DataAanvraag.asmx/standaardDataRequest";
        foreach ($postDataGenerated as $key => $postData) {
            $_connect = fsockopen('ssl://'.$connectionHost, 443, $errno, $errstr);
            if (!$_connect)
            {
                die('Geen verbinding');
            }
            fputs($_connect, "POST ".$connectionPath." HTTP/1.1\r\n");
            fputs($_connect, "Host: ".$connectionHost."\r\n");
            fputs($_connect, "Content-Type: application/x-www-form-urlencoded\r\n");
            fputs($_connect, "Content-Length: ".strlen($postData)."\r\n");
            fputs($_connect, "Connection: close\r\n");
            fputs($_connect, "\r\n"); // all headers sent
            fputs($_connect, $postData);
            $bCatch = false;

            $postResult = '';
            while (!feof($_connect))
            {
                $line = fgets($_connect, 128);
                if ($bCatch == true)
                    $postResult .= $line;
                if ($line == "\r\n")
                    $bCatch = true;
            }


            $parsed[$key] = htmlspecialchars_decode($postResult);
            fclose($_connect);
        }



        if (!is_dir('storage/pdf')) {
            mkdir('storage/pdf');
        }

        $fileName = $carNumber . '.pdf';
            $pdf = PDF::loadView('kenteken.pdf', [
        ])->save("storage/pdf/$fileName");

        return view('kenteken/viewPDF',[
            'carNumber' => $carNumber
        ]);

        $pdf = PDF::loadView('kenteken.pdf', [
            //'result' => $result,
        ]);



        //return $pdf->download('file.pdf');


    }

    public function pdfSubmit() {
//        $carNumber = request()->kentekenInput;
//        session(['carNumber' => $carNumber]);
        $carNumber = session('carNumber', '');

        if (auth()->user()) {
            if (auth()->user()->credits > 0) {
                return redirect()->action('KentekenController@showPDF');
            }
            return redirect()->action('KentekenController@payPDF');
        } else {
            return redirect()->action('KentekenController@buyPDF');
        }
    }

    public function buyPDF() {
        return view('kenteken/buyPDF');
    }

    public function submitBuyPDF() {
        $email = request()->email;
        $password = request()->password;
        $users = User::where('email', $email)
            ->get();
        $usersCount = User::where('email', $email)
            ->count();

        if ($usersCount) {
            if (Hash::check($password, $users[0]->password)) {
                $user = $users[0];
                Auth::login($user, true);
            }
            else {
                return redirect()->action('KentekenController@buyPDF');
            }
        } else {
            $user = new User(request()->all());
            $user->password = Hash::make(request()->password);
            $user->credits = 0;
            $user->save();
            Auth::login($user, true);
        }

        return redirect()->action('KentekenController@payPDF');
    }

    public function payPDF() {
        if(Auth::guest()) {
            return redirect()->action('KentekenController@buyPDF');
        } else {
            return view('kenteken/payPDF');
        }
    }

    public function submitPayPDF() {
        $mollie = new \Mollie\Api\MollieApiClient();
        $mollie->setApiKey("test_RF6PtAcCGJkvUrmK6MdJqtPMtyN2ar");

        $euros = 0.00;
        if (request()->credit == 1) {
            $credits = 5;
            $euros = "5.00";
        } else if (request()->credit == 2) {
            $credits = 15;
            $euros = "12.00";
        } else if (request()->credit == 3) {
            $credits = 40;
            $euros = "28.00";
        }





        $payment = $mollie->payments->create([
            "amount" => [
                "currency" => "EUR",
                "value" => $euros
            ],
            "method" => 'creditcard',
            "description" => "Payment for kenteken",
            "redirectUrl" => "http://kenteken/kenteken/confirm-mollie/",
            "webhookUrl"  => "https://webhook/",
        ]);

        $user = auth()->user();
        $user->credits_add = $credits;
        $user->mollie_id = $payment->id;
        $user->save();

        $payment = $mollie->payments->get($payment->id);

        //header("Location: " . $payment->getCheckoutUrl(), true, 303);
        return redirect($payment->getCheckoutUrl(), 303);
    }

    public function confirmMollie() {
        $mollie = new \Mollie\Api\MollieApiClient();
        $mollie->setApiKey("test_RF6PtAcCGJkvUrmK6MdJqtPMtyN2ar");

        $user = auth()->user();
        $payment = $payment = $mollie->payments->get($user->mollie_id);
        if($payment->status == 'paid') {
            $user->credits += $user->credits_add;
            $user->credits_add = 0;
            $user->mollie_id = null;
            $user->save();
            return redirect()->action('KentekenController@showPDF');
        }

        return view('kenteken/error');
    }

    public function showPDF() {
        if(Auth::guest()) {
            return redirect()->action('KentekenController@buyPDF');
        } else {
            $user = auth()->user();

            if ($user->credits <= 0) {
                return view('kenteken/error');
            }

            $user->credits--;
            $user->bought = 1;
            $user->save();
            
            $carNumber = session('carNumber', '');

            //constants
            $vweUser = 'Cell01';
            $vwePassword = 'nyx66M';
            $vweTypes = ['SB-ATL-MMT', 'SB-ATL-OPTIE-FABRIEK', 'SB-ATL-PP', 'SB-MILIEU-BASIC', 'SB-MRB', 'SB-OKR-CHECK', 'SB-RDW-ADVANCED', 'SB-RDW-HIST-ADV', 'SB-RDW-IMPORTLAND', 'SB-ATD-TCO', 'SB-ATD-TYRE-INFO', 'SB-ATL-TAX-ONLINE', 'SB-RDW-HIST-APK', 'SB-RDW-OVIZ-REAL', 'SB-RDW-STATUS-HIST', 'SB-RDW-ADVANCED', 'SB-STAT-WP-STA-BEZIT'];

            /*if (file_exists('storage/pdf/' . $carNumber . '.pdf')) {
                if (time() - filemtime("storage/pdf/$carNumber.pdf") < 14 * 24 * 60 * 60) {
                    //echo "<a href=". "/storage/pdf/$carNumber.pdf" .">download</a>";
                    return view('kenteken/viewPDF', [
                        'carNumber' => $carNumber
                    ]);
                }
            } else {
                Storage::delete("public/pdf/$carNumber.pdf");
            }*/

            $postDataGenerated = [];
            foreach($vweTypes as $item) {
                $request =
                    "requestXml=<bericht>
                    <authenticatie>
                        <naam>$vweUser</naam>
                        <wachtwoord>$vwePassword</wachtwoord>
                        <berichtsoort>$item</berichtsoort>
                        <referentie></referentie>
                    </authenticatie>
                    <parameters>
                        <kenteken>$carNumber</kenteken>
                    </parameters>
                    </bericht>";
                $postDataGenerated[$item] = $request;
            }

            global $parsed;
            $parsed = [];

            //receiving information
            $connectionHost = "acceptatie-interdata.vwe.nl";
            $connectionPath = "/DataAanvraag.asmx/standaardDataRequest";
            foreach ($postDataGenerated as $key => $postData) {
                $_connect = fsockopen('ssl://'.$connectionHost, 443, $errno, $errstr);
                if (!$_connect)
                {
                    die('Geen verbinding');
                }
                fputs($_connect, "POST ".$connectionPath." HTTP/1.1\r\n");
                fputs($_connect, "Host: ".$connectionHost."\r\n");
                fputs($_connect, "Content-Type: application/x-www-form-urlencoded\r\n");
                fputs($_connect, "Content-Length: ".strlen($postData)."\r\n");
                fputs($_connect, "Connection: close\r\n");
                fputs($_connect, "\r\n"); // all headers sent
                fputs($_connect, $postData);
                $bCatch = false;
                $postResult = '';
                while (!feof($_connect))
                {
                    $line = fgets($_connect, 128);
                    if ($bCatch == true)
                        $postResult .= $line;
                    if ($line == "\r\n")
                        $bCatch = true;
                }
                $parsed[$key] = htmlspecialchars_decode($postResult);
                fclose($_connect);
            }
            if (!is_dir('storage/pdf')) {
                mkdir('storage/pdf');
            }
            $fileName = $carNumber . '.pdf';
            $pdf = PDF::loadView('kenteken.pdf', [
            ])->save("storage/pdf/$fileName");

            $buypdf = new Buypdf([
                'user_id' => $user->id,
                'plate' => $carNumber
            ]);
            $buypdf->save();

            return view('kenteken/viewPDF',[
                'carNumber' => $carNumber
            ]);
        }
    }

    public function showShortPDF() {

        if(Auth::guest()) {
            return redirect()->action('KentekenController@buyPDF');
        } else {
            $user = auth()->user();

            $carNumber = request()->kentekenInput;
            session(['carNumber' => $carNumber]);

            //constants
            $vweUser = 'Cell01';
            $vwePassword = 'nyx66M';
//            $vweTypes = ['SB-ATL-MMT', 'SB-ATL-OPTIE-FABRIEK', 'SB-ATL-PP', 'SB-MILIEU-BASIC', 'SB-MRB', 'SB-OKR-CHECK', 'SB-RDW-ADVANCED', 'SB-RDW-HIST-ADV', 'SB-RDW-IMPORTLAND', 'SB-ATD-TCO', 'SB-ATD-TYRE-INFO', 'SB-ATL-TAX-ONLINE', 'SB-RDW-HIST-APK', 'SB-RDW-OVIZ-REAL', 'SB-RDW-STATUS-HIST', 'SB-RDW-ADVANCED', 'SB-STAT-WP-STA-BEZIT'];
            $vweTypes = ['SB-ATL-MMT'];

            $postDataGenerated = [];
            foreach ($vweTypes as $item) {
                $request =
                    "requestXml=<bericht>
                    <authenticatie>
                        <naam>$vweUser</naam>
                        <wachtwoord>$vwePassword</wachtwoord>
                        <berichtsoort>$item</berichtsoort>
                        <referentie></referentie>
                    </authenticatie>
                    <parameters>
                        <kenteken>$carNumber</kenteken>
                    </parameters>
                    </bericht>";
                $postDataGenerated[$item] = $request;
            }

            global $parsed;
            $parsed = [];

            //receiving information
            $connectionHost = "acceptatie-interdata.vwe.nl";
            $connectionPath = "/DataAanvraag.asmx/standaardDataRequest";
            foreach ($postDataGenerated as $key => $postData) {
                $_connect = fsockopen('ssl://' . $connectionHost, 443, $errno, $errstr);
                if (!$_connect) {
                    die('Geen verbinding');
                }
                fputs($_connect, "POST " . $connectionPath . " HTTP/1.1\r\n");
                fputs($_connect, "Host: " . $connectionHost . "\r\n");
                fputs($_connect, "Content-Type: application/x-www-form-urlencoded\r\n");
                fputs($_connect, "Content-Length: " . strlen($postData) . "\r\n");
                fputs($_connect, "Connection: close\r\n");
                fputs($_connect, "\r\n"); // all headers sent
                fputs($_connect, $postData);
                $bCatch = false;
                $postResult = '';
                while (!feof($_connect)) {
                    $line = fgets($_connect, 128);
                    if ($bCatch == true)
                        $postResult .= $line;
                    if ($line == "\r\n")
                        $bCatch = true;
                }
                $parsed[$key] = htmlspecialchars_decode($postResult);
                fclose($_connect);
            }

            return view('kenteken/shortPDF', [
                'carNumber' => $carNumber
            ]);
        }
    }

    public function cabinet() {
        $user_id = auth()->user()->id;
        $buypdfs = Buypdf::where('user_id', $user_id)
            ->get();
        $pdfs = [];
        foreach ($buypdfs as $item) {
            $pdfs[] = $item->plate;
        }

        $credits = User::find($user_id)->credits;
        
        return view('kenteken/cabinet',[
            'pdfs' => $pdfs,
            'credits' => $credits
        ]);
    }
}