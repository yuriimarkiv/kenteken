<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Good;

class GoodController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $goods = Good::orderBy('id', 'desc')->paginate(24);

        return view('goods.index', [
            'goods' => $goods,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('goods.form', [
            'good' => new Good()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            /*'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',*/
        ]);

        $good = new Good($request->all());

        //file storage
        if ($request->hasFile('image')) {
            $good->photo = $request->file('image')->store('public/goods');
            $good->photo = str_replace('public/', '', $good->photo);
        }

        $good->save();

        return redirect()->action('GoodController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $good = Good::find($id);

        return view('goods.show', [
            'good' => $good
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $good = Good::find($id);

        return view('goods.form', [
            'good' => $good,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
        ]);*/

        $good = Good::find($id);
        $good->update($request->all());

        //file replacing
        if ($request->hasFile('image')) {
            if ($good->photo) {
                Storage::delete("public/$good->photo");
            }
            $good->photo = $request->file('image')->store('public/goods');
            $good->photo = str_replace('public/', '', $good->photo);
        }

        $good->save();

        return redirect()->action('GoodController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $good= Good::find($id);
        $good->delete();
        return redirect()->back();
    }
}
