<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Good;
use App\Order;
use App\Buy;
use App\OrderBuy;
use Illuminate\Support\Facades\Auth;

class BuyController extends Controller {
    public function buy()
    {
        $goods = Good::orderBy('id', 'desc')->paginate(24);

        return view('buy/buy', [
            'goods' => $goods,
        ]);
    }

    public function addToCart($id) {
        $goodsInCart = session('goodsInCart', '');
        $goodsInCart = unserialize($goodsInCart);
        if (empty($goodsInCart)) {
            $goodsInCart[$id] = 1;
        } else {
            if (key_exists($id, $goodsInCart)) {
                $goodsInCart[$id]++;
            } else {
                $goodsInCart[$id] = 1;
            }
        }


        $goodsInCart = serialize($goodsInCart);

        session(['goodsInCart' => $goodsInCart]);

        return redirect()->action('BuyController@buy');
    }

    public function cart() {
        $goodsInCart = session('goodsInCart', '');
        $goods = unserialize($goodsInCart);
        $order = new Order();
        return view('buy/cart', [
            'goods' => $goods,
            'order' => $order
        ]);
    }

    public function confirm(Request $request) {
        $order = new Order($request->all());
        $order->status = 'New';
        if (Auth::id()) {
            $order->user_id = Auth::id();
        }
        $order->save();

        $goodsInCart = session('goodsInCart', '');
        $goods = unserialize($goodsInCart);
        foreach($goods as $id => $count) {
            $good = Good::find($id);
            $buy = new Buy([
                'price' => $good->price,
                'count' => $count,
                'good_id' => $id
            ]);
            $buy->save();

            $orderBuy = new OrderBuy([
                'order_id' => $order->id,
                'buy_id' => $buy->id
            ]);

            $orderBuy->save();
        }
    }

    public function deleteFromCart($id)
    {
        $goodsInCart = session('goodsInCart', '');
        $goodsInCart = unserialize($goodsInCart);
        unset($goodsInCart[$id]);

        $goodsInCart = serialize($goodsInCart);
        session(['goodsInCart' => $goodsInCart]);
    }

    public function clearCart()
    {
        session(['goodsInCart' => []]);
    }
}