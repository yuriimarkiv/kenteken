<?php

namespace App\Http\Controllers;

use App\Statistics;

use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function index() {
        $numberClients = Statistics::numberClients();
        $numberFreeCheck = Statistics::numberFreeCheck();
        $numberBought = Statistics::numberBought();
        $clientsTimeline = Statistics::clientsTimeline();

        $numberClientsMonth = Statistics::numberClientsMonth();
        $numberFreeCheckMonth = Statistics::numberFreeCheckMonth();
        $numberBoughtMonth = Statistics::numberBoughtMonth();
        $clientsTimelineMonth = Statistics::clientsTimelineMonth();

        return view('statistics/index',[
            'numberClients' => $numberClients,
            'numberFreeCheck' => $numberFreeCheck,
            'numberBought' => $numberBought,
            'clientsTimeline' => $clientsTimeline,
            'numberClientsMonth' => $numberClientsMonth,
            'numberFreeCheckMonth' => $numberFreeCheckMonth,
            'numberBoughtMonth' => $numberBoughtMonth,
            'clientsTimelineMonth' => $clientsTimelineMonth
        ]);
    }

    public function analytics() {
        $uniqueVisitors = Statistics::uniqueVisitors();
        $referrals = Statistics::referrals();
        $topKeywords = Statistics::topKeywords();
        $bounce = Statistics::bounce();
        $leaves = Statistics::leaves();
        $topPages = Statistics::topPages();

        return view('statistics/analytics',[
            'uniqueVisitors' => $uniqueVisitors,
            'referrals' => $referrals,
            'topKeywords' => $topKeywords,
            'bounce' => $bounce,
            'leaves' => $leaves,
            'topPages' => $topPages
        ]);
    }

    public function traffic() {
        $organicSearch = Statistics::organicSearch();
        $referral = Statistics::referral();
        $direct = Statistics::direct();
        $social = Statistics::social();
        $conversionOrganicSearch = Statistics::conversionOrganicSearch();
        $conversionReferral = Statistics::conversionReferral();
        $conversionDirect = Statistics::conversionDirect();
        $conversionSocial = Statistics::conversionSocial();

        return view('statistics/traffic',[
            'organicSearch' => $organicSearch,
            'referral' => $referral,
            'direct' => $direct,
            'social' => $social,
            'conversionOrganicSearch' => $conversionOrganicSearch,
            'conversionReferral' => $conversionReferral,
            'conversionDirect' => $conversionDirect,
            'conversionSocial' => $conversionSocial
        ]);
    }
}
