<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buy extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price',
        'count',
        'good_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function good()
    {
        return $this->belongsTo('App\Good');
    }
}
