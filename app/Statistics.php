<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buypdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Statistics extends Model
{
    //for index page
    public static function numberClients() {
        $users = User::get();
        $count = 0;
        foreach($users as $user) {
            if(!$user->can('admin')) {
                $count++;
            }
        }

        return $count;
    }

    public static function numberFreeCheck() {
        $users = User::get();
        $count = 0;
        foreach($users as $user) {
            if(!$user->can('admin') && $user->bought == 0) {
                $count++;
            }
        }

        return $count;
    }

    public static function numberBought() {
        $users = User::get();
        $count = 0;
        foreach($users as $user) {
            if(!$user->can('admin') && $user->bought == 1) {
                $count++;
            }
        }

        return $count;
    }

    public static function clientsTimeline() {
        $buypdf = Buypdf::get();

        $arr =[];

        foreach ($buypdf as $item) {
            $time = date('H', strtotime($item->created_at));
            if (array_key_exists($time, $arr)) {
                $arr[$time]++;
            } else {
                $arr[$time] = 1;
            }

        }
        ksort($arr);
        return $arr;
    }

    public static function numberClientsMonth() {
        $users = User::
            where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();
        $count = 0;
        foreach($users as $user) {
            if(!$user->can('admin')) {
                $count++;
            }
        }

        return $count;
    }

    public static function numberFreeCheckMonth() {
        $users = User::
            where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();
        $count = 0;
        foreach($users as $user) {
            if(!$user->can('admin') && $user->bought == 0) {
                $count++;
            }
        }

        return $count;
    }

    public static function numberBoughtMonth() {
        $users = User::
            where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();
        $count = 0;
        foreach($users as $user) {
            if(!$user->can('admin') && $user->bought == 1) {
                $count++;
            }
        }

        return $count;
    }

    public static function clientsTimelineMonth() {
        $buypdf = Buypdf::
            where('created_at', '>=', Carbon::now()->startOfMonth())
            ->get();

        $arr =[];

        foreach ($buypdf as $item) {
            $time = date('H', strtotime($item->created_at));
            if (array_key_exists($time, $arr)) {
                $arr[$time]++;
            } else {
                $arr[$time] = 1;
            }

        }
        ksort($arr);
        return $arr;
    }

    //for analytics stats page
    public static function uniqueVisitors() {
        $users = User::get()->count();
        return $users;
    }

    public static function referrals() {
        return 2;
    }

    public static function topKeywords() {
        $words = Buypdf::select(['plate'])
            ->orderBy(DB::raw('count(plate)'), 'DESC')
            ->groupBy('plate')
            ->limit(20)
            ->get();
        $answer = [];
        foreach($words as $word) {
            $answer[] = $word->plate;
        }
        return $answer;
    }

    public static function bounce() {
        return 4;
    }

    public static function leaves() {
        return 5;
    }

    public static function topPages() {
        return 6;
    }

    //for traffic pages
    public static function organicSearch() {
        return 1;
    }

    public static function referral() {
        return 2;
    }

    public static function direct() {
        return 3;
    }

    public static function social() {
        return 4;
    }

    public static function conversionOrganicSearch() {
        return 5;
    }

    public static function conversionReferral() {
        return 6;
    }

    public static function conversionDirect() {
        return 7;
    }

    public static function conversionSocial() {
        return 8;
    }
}
