<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'photo' => 'mimes:jpeg,jpg,png | max:1000',
        'price'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
