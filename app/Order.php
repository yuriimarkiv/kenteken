<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'status',
        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function b()
    {
        $order_buys = OrderBuy::select(['buy_id'])
            ->where('order_id', $this->id)
            ->get();

        $buys = [];

        foreach($order_buys as $item) {
            $buys[] = Buy::find($item['buy_id']);
        }

        return $buys;
    }
}
