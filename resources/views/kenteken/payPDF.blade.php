@extends('common-tamplate')

@section('content')


<div class="row">
    <div class="col-md-6 offset-md-3">
        <h3>Want to see more? Please buy credits</h3>
        <form action="{{url('/kenteken/submitPayPDF')}}" method="post">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Select a package</label>
                    <br/>
                    <input type="radio" class="" id="credit1" name="credit" value="1"/>Buy 5 credits for 5 euro<br/>
                    <input type="radio" class="" id="credit2" name="credit" value="2"/>Buy 15 credits for 12 euro<br/>
                    <input type="radio" class="" id="credit3" name="credit" value="3"/>Buy 40 credits for 28 euro<br/>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop