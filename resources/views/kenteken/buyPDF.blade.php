@extends('common-tamplate')

@section('content')
<div class="row">
    <div class="col-md-6 offset-md-3">
        <h3>Want to see more? Please buy credits</h3>
        <form action="{{url('/kenteken/submitBuyPDF')}}" method="post">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" id="name" name="name"/>
                </div>
                <div class="form-group">
                    <label for="name">Email</label>
                    <input class="form-control" id="email" name="email"/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" id="password" name="password"/>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop