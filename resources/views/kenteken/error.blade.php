@extends('common-tamplate')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h1>
                Error occured. There is not enough credits or payment error. Try again
            </h1>
        </div>
    </div>
@stop