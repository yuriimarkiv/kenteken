@extends('common-tamplate')

@section('content')




<div class="row">
    <div class="col-md-6 offset-md-3">

        <?php
        function findParameter($string, $tag) {
            $endTag = $tag[0] . '/' . substr($tag, 1);
            $startPos = strpos($string, $tag);
            if ($startPos === false) {
                return '';
            }
            $startPos += strlen($tag);
            $endPos = strpos($string, $endTag);
            $value = substr($string, $startPos, $endPos - $startPos);
            return $value;
        }

        function findParameterWithAttributes($string, $tag) {
            $endTag = $tag[0] . '/' . substr($tag, 1);
            $startPos = strpos($string, $tag);
            if ($startPos === false) {
                return '';
            }
            $startPos += strlen($tag);
            $endPos = strpos($string, $endTag);
            $value = substr($string, $startPos, $endPos - $startPos);
            $pos = strpos($value, '>');
            $value = substr($value, $pos + 1);
            return $value;
        }

        function findParameterList($string, $tag) {
            $endTag = $tag[0] . '/' . substr($tag, 1);
            $lastPos = 0;
            $values = [];
            while (($lastPos = strpos($string, $tag, $lastPos)) !== false) {
                $startPos = $lastPos;
                $startPos += strlen($tag);
                $endPos = strpos($string, $endTag, $lastPos);
                $value = substr($string, $startPos, $endPos - $startPos);
                $values[] = $value;
                $lastPos = $lastPos + strlen($tag);
            }

            return $values;
        }

        function findParameterListWithAttributes($string, $tag) {
            $endTag = $tag[0] . '/' . substr($tag, 1);
            $lastPos = 0;
            $values = [];
            while (($lastPos = strpos($string, $tag, $lastPos)) !== false) {
                $startPos = $lastPos;
                $startPos += strlen($tag);
                $endPos = strpos($string, $endTag, $lastPos);
                $value = substr($string, $startPos, $endPos - $startPos);
                $pos = strpos($value, '>');
                $value = substr($value, $pos + 1);
                $values[] = $value;
                $lastPos = $lastPos + strlen($tag);
            }

            return $values;
        }

        global $parsed;



        $params = [];

        //SB-ATL-MMT
        $param['kenteken'] = findParameter($parsed['SB-ATL-MMT'], '<atl:kenteken>');
        $params['kenteken'] = findParameter($parsed['SB-ATL-MMT'], '<atl:kenteken>');
        $params['merk'] = findParameter($parsed['SB-ATL-MMT'], '<atl:merk>');
        $params['model'] = findParameter($parsed['SB-ATL-MMT'], '<atl:model>');
        $params['atlCode'] = findParameter($parsed['SB-ATL-MMT'], '<atl:atlCode>');
        $params['uitvoering'] = findParameter($parsed['SB-ATL-MMT'], '<atl:uitvoering>');
        $params['uitvoering_lang'] = findParameter($parsed['SB-ATL-MMT'], '<atl:uitvoering_lang>');
        $params['fabrieksCode'] = findParameter($parsed['SB-ATL-MMT'], '<atl:fabrieksCode>');



        if($params['kenteken'] == '') {
            echo 'There is no data for such plate number';
            die();
        }

        $carNumber = $param['kenteken'];
        ?>

        <img src="{{ url('/')."/storage/images/logo.png" }}" class="image-logo" alt="site logo" />

        <div class="reportdatum">reportdatum: <?=date("d-m-Y")?></div>

        <h3>General vehicle information</h3>

        {{--<h1>MMT</h1>--}}
        <table class="formatted-table formatted-table-size">
            <tr>
                <td>atlCode</td>
                <td><?=$params['atlCode']?></td>
            </tr>
            <tr>
                <td>uitvoering</td>
                <td><?=$params['uitvoering']?></td>
            </tr>
            <tr>
                <td>uitvoering_lang</td>
                <td><?=$params['uitvoering_lang']?></td>
            </tr>
            <tr>
                <td>fabrieksCode</td>
                <td><?=$params['fabrieksCode']?></td>
            </tr>
        </table>


            <form class="kenteken-form" action="{{url('/kenteken/pdfSubmit')}}" method="post">
                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                <div class="form-group">
                    <label for="kenteken-input">Kenteken</label>
                    <input type="text" class="form-control kenteken-input" id="kenteken-input" name="kentekenInput" aria-describedby="kenteken-input-help" placeholder="{{$carNumber}}" disabled>
                    <small id="kenteken-input-help" class="form-text text-muted">vul hier het kenteken in:</small>
                </div>
                <button type="submit" class="btn btn-primary kenteken-submit-btn">View more</button>
            </form>


                <style>
                    .energy-label-picture {
                        width: 200px;
                    }

                    .car-damage-picture {
                        max-width: 150px;
                    }

                    .formatted-table-size {
                        width: 100%;
                        table-layout: fixed;
                    }

                    .formatted-table
                    {
                        margin: 0 0;
                        border-collapse: collapse;
                        text-align: left;
                    }
                    .formatted-table th
                    {
                        font-size: 14px;
                        font-weight: bold;
                        padding: 4px 0;
                        border-bottom: 1px solid #ccc;
                    }
                    .formatted-table td
                    {
                        font-size: 14px;
                        border-bottom: 1px solid #ccc;
                        padding: 4px 0;
                    }

                    .formatted-table tr:nth-child(even) {
                        background-color: #f7f7f7
                    }

                    h1 {
                        font-size: 14pt;
                        border-bottom: 3px solid #0396c5;
                        margin-top: 20px;
                    }
                    h2 {
                        font-size: 14px;
                    }

                    .image-logo {
                        width: 250px;
                    }

                    .page_break {
                        page-break-before: always;
                    }

                    .bandenmaten-table {
                        width: 50%;
                        float: left;
                    }

                    .bandenmaten-pic {
                        max-height:80px;
                    }

                    .plate-number-container {
                        position: relative;
                        text-align: center;
                    }

                    .plate-number-text {
                        position: absolute;
                        top: 14px;
                        left: 50%;
                        transform: translate(-50%, -50%);
                        font-size: 50px;
                    }

                    .reportdatum {
                        text-align: center;
                    }
                </style>

    </div>
</div>


@stop