@extends('common-tamplate')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div>
                Credits left: {{$credits}}
            </div>
            <br/>
            <ul>
            @foreach($pdfs as $pdf)
                <li><a href="/storage/pdf/{{$pdf}}.pdf">{{$pdf}}</a></li>
            @endforeach
            </ul>
        </div>
    </div>
@stop