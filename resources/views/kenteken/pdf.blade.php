<?php
function findParameter($string, $tag) {
    $endTag = $tag[0] . '/' . substr($tag, 1);
    $startPos = strpos($string, $tag);
    if ($startPos === false) {
        return '';
    }
    $startPos += strlen($tag);
    $endPos = strpos($string, $endTag);
    $value = substr($string, $startPos, $endPos - $startPos);
    return $value;
}

function findParameterWithAttributes($string, $tag) {
    $endTag = $tag[0] . '/' . substr($tag, 1);
    $startPos = strpos($string, $tag);
    if ($startPos === false) {
        return '';
    }
    $startPos += strlen($tag);
    $endPos = strpos($string, $endTag);
    $value = substr($string, $startPos, $endPos - $startPos);
    $pos = strpos($value, '>');
    $value = substr($value, $pos + 1);
    return $value;
}

function findParameterList($string, $tag) {
    $endTag = $tag[0] . '/' . substr($tag, 1);
    $lastPos = 0;
    $values = [];
    while (($lastPos = strpos($string, $tag, $lastPos)) !== false) {
        $startPos = $lastPos;
        $startPos += strlen($tag);
        $endPos = strpos($string, $endTag, $lastPos);
        $value = substr($string, $startPos, $endPos - $startPos);
        $values[] = $value;
        $lastPos = $lastPos + strlen($tag);
    }

    return $values;
}

function findParameterListWithAttributes($string, $tag) {
    $endTag = $tag[0] . '/' . substr($tag, 1);
    $lastPos = 0;
    $values = [];
    while (($lastPos = strpos($string, $tag, $lastPos)) !== false) {
        $startPos = $lastPos;
        $startPos += strlen($tag);
        $endPos = strpos($string, $endTag, $lastPos);
        $value = substr($string, $startPos, $endPos - $startPos);
        $pos = strpos($value, '>');
        $value = substr($value, $pos + 1);
        $values[] = $value;
        $lastPos = $lastPos + strlen($tag);
    }

    return $values;
}

global $parsed;

/*$parsed = [
    "SB-ATL-MMT" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-ATL-MMT&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323265</interdataReferentie></resultaat><rubrieken><atlMmtInfo xsi:type=\"atl:atlMmtInfoTypeV3\" xmlns:atl=\"http://www.xmlmode.nl/interdata/atl\"><atl:kenteken>61NVL2</atl:kenteken><atl:merk>Skoda</atl:merk><atl:model>Octavia</atl:model><atl:uitvoeringen><atl:uitvoeringItem ranking=\"1\"><atl:atlCode>90692</atl:atlCode><atl:uitvoering>1.8 TSI Ambition</atl:uitvoering><atl:uitvoering_lang>1.8 TSI Ambition</atl:uitvoering_lang><atl:fabrieksCode>1Z3</atl:fabrieksCode></atl:uitvoeringItem></atl:uitvoeringen></atlMmtInfo></rubrieken></antwoordbericht>",
    "SB-ATL-OPTIE-FABRIEK" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-ATL-OPTIE-FABRIEK&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323332</interdataReferentie></resultaat><rubrieken><atlOptieFabriek xsi:type=\"atl:atlOptieFabriekTypeV1\" xmlns:atl=\"http://www.xmlmode.nl/interdata/atl\"><atl:kenteken>61NVL2</atl:kenteken><atl:atlCode>90692</atl:atlCode><atl:opties><atl:optie id=\"242\" verkort=\"alarmsysteem\" bedrag=\"300\">alarmsysteem</atl:optie><atl:optie id=\"1028\" verkort=\"audioinst. + CD-wisselaar\" bedrag=\"850\">audioinstallatie met CD-wisselaar</atl:optie><atl:optie id=\"1471\" verkort=\"Bluetooth telefoonvoorb.\" bedrag=\"530\">Bluetooth telefoonvoorbereiding</atl:optie><atl:optie id=\"269\" verkort=\"el. climate control\" bedrag=\"550\">electronic climate controle</atl:optie><atl:optie id=\"12\" verkort=\"elektr.schuif-/kanteldak\" bedrag=\"990\">elektrisch schuif-/kanteldak</atl:optie><atl:optie id=\"364\" verkort=\"elektrische ramen achter\" bedrag=\"210\">elektrische ramen achter</atl:optie><atl:optie id=\"1470\" verkort=\"ESP + ESD\" bedrag=\"650\">ESP en ESD</atl:optie><atl:optie id=\"179\" verkort=\"led. stuurw+versn. pook\" bedrag=\"250\">lederen stuurwiel en versnellingspook</atl:optie><atl:optie id=\"1006\" verkort=\"lichtmetalen velgen 15&quot;\" bedrag=\"550\">lichtmetalen velgen 15\"</atl:optie><atl:optie id=\"1019\" verkort=\"lichtmetalen velgen 16&quot;\" bedrag=\"650\">lichtmetalen velgen 16\"</atl:optie><atl:optie id=\"1023\" verkort=\"lichtmetalen velgen 17&quot;\" bedrag=\"890\">lichtmetalen velgen 17\"</atl:optie><atl:optie id=\"1\" verkort=\"metaalkleur\" bedrag=\"560\">metaalkleur</atl:optie><atl:optie id=\"1137\" verkort=\"multifunctioneel stuur\" bedrag=\"220\">stuurwiel multifunctioneel</atl:optie><atl:optie id=\"1442\" verkort=\"multimedia-voorbereiding\" bedrag=\"160\">multimedia-voorbereiding</atl:optie><atl:optie id=\"1449\" verkort=\"navigatie full map + hdd\" bedrag=\"2000\">navigatiesysteem full map + hard disk</atl:optie><atl:optie id=\"288\" verkort=\"navigatiesysteem\" bedrag=\"700\">navigatiesysteem</atl:optie><atl:optie id=\"1161\" verkort=\"parkeersensor achter\" bedrag=\"410\">parkeersensor achter</atl:optie><atl:optie id=\"94\" verkort=\"verwarmde voorstoelen\" bedrag=\"230\">voorstoelen verwarmd</atl:optie><atl:optie id=\"1118\" verkort=\"zonnescherm achterruit\" bedrag=\"130\">zonnescherm achterruit</atl:optie></atl:opties></atlOptieFabriek></rubrieken></antwoordbericht>",
    "SB-ATL-PP" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-ATL-PP&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323377</interdataReferentie></resultaat><rubrieken><atlInfoPp xsi:type=\"atl:atlInfoPpTypeV1\" xmlns:atl=\"http://www.xmlmode.nl/interdata/atl\"><atl:gewicht>1250</atl:gewicht><atl:treinGewicht xsi:nil=\"true\" /><atl:aandrijving code=\"V\">Voorwiel</atl:aandrijving><atl:carrosserieType>Hatchb.</atl:carrosserieType><atl:aantalDeuren>5</atl:aantalDeuren><atl:acceleratie>78</atl:acceleratie><atl:sportiviteitsKlasse>19.45</atl:sportiviteitsKlasse><atl:topsnelheid>223</atl:topsnelheid><atl:emissieCode>EURO5</atl:emissieCode><atl:energielabel>C</atl:energielabel><atl:vermogenKw>118</atl:vermogenKw><atl:cilinderinhoud>1798</atl:cilinderinhoud><atl:brandstof code=\"B\">Benzine</atl:brandstof><atl:versnellingsType>H6</atl:versnellingsType><atl:aantalZitplaatsen>5</atl:aantalZitplaatsen></atlInfoPp></rubrieken></antwoordbericht>",
    "SB-MILIEU-BASIC" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-MILIEU-BASIC&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323364</interdataReferentie></resultaat><rubrieken><milieuInfoBasic xsi:type=\"mil:milieuInfoBasicTypeV3\" xmlns:mil=\"http://www.xmlmode.nl/interdata/milieu\"><mil:kenteken>61NVL2</mil:kenteken><mil:roetfilter>n.v.t.</mil:roetfilter><mil:roetdeeltjes>0.002</mil:roetdeeltjes><mil:co2uitstootGecombineerd>158</mil:co2uitstootGecombineerd><mil:euroklasse>Euro 5</mil:euroklasse><mil:energielabel>C</mil:energielabel><mil:brandstofverbruikStad>9.50</mil:brandstofverbruikStad><mil:brandstofverbruikBuitenweg>5.50</mil:brandstofverbruikBuitenweg><mil:brandstofverbruikGecombineerd>6.90</mil:brandstofverbruikGecombineerd><mil:typeVersnellingsbak code=\"H\">Handmatig geschakelde transmissie</mil:typeVersnellingsbak><mil:aantalVersnellingen>6</mil:aantalVersnellingen><mil:topsnelheid>0</mil:topsnelheid><mil:bijtellingsKlasse>25%</mil:bijtellingsKlasse><mil:coEmissie>0.41700</mil:coEmissie><mil:hcEmissie>0.06400</mil:hcEmissie><mil:noxEmissie>0.01920</mil:noxEmissie><mil:hcNoxEmissie xsi:nil=\"true\" /><mil:geluidsniveauStilstaand>79</mil:geluidsniveauStilstaand><mil:toerenGeluidsniveauStilstaand>3800</mil:toerenGeluidsniveauStilstaand><mil:geluidsniveauRijdend xsi:nil=\"true\" /></milieuInfoBasic></rubrieken></antwoordbericht>",
    "SB-MRB" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-MRB&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323383</interdataReferentie></resultaat><rubrieken><wegenbelasting xsi:type=\"mrb:wegenbelastingTypeV2\" xmlns:mrb=\"http://www.xmlmode.nl/interdata/mrb\"><mrb:waarden><mrb:provincie>Noord-Holland</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>136.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Friesland</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>137.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Flevoland</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>143.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Zuid-Holland</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>152.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Limburg</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>143.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Gelderland</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>151.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Utrecht</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>139.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Groningen</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>150.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Noord-Brabant</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>142.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Zeeland</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>146.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Drenthe</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>153.00</mrb:bedragKwartaal></mrb:waarden><mrb:waarden><mrb:provincie>Overijssel</mrb:provincie><mrb:gewichtVanaf>1151</mrb:gewichtVanaf><mrb:gewichtTotEnMet>1250</mrb:gewichtTotEnMet><mrb:bedragKwartaal>144.00</mrb:bedragKwartaal></mrb:waarden></wegenbelasting></rubrieken></antwoordbericht>",
    "SB-OKR-CHECK" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-OKR-CHECK&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>20</code><omschrijving>Tellerstand vandaag reeds gecontroleerd.</omschrijving><interdataReferentie>1040323403</interdataReferentie></resultaat><rubrieken /></antwoordbericht>",
    "SB-RDW-ADVANCED" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-RDW-ADVANCED&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323406</interdataReferentie></resultaat><rubrieken><rdwInfoAdvanced xsi:type=\"rdw:rdwInfoAdvancedTypeV8\" xmlns:rdw=\"http://www.xmlmode.nl/interdata/rdw\"><rdw:kenteken>61NVL2</rdw:kenteken><rdw:isMeldCodeCorrect>false</rdw:isMeldCodeCorrect><rdw:kentekenSignaal xsi:nil=\"true\" /><rdw:merk code=\"SKOD\">skoda</rdw:merk><rdw:handelsbenaming>OCTAVIA</rdw:handelsbenaming><rdw:voertuigsoort>P</rdw:voertuigsoort><rdw:brandstof1 code=\"B\">benzine</rdw:brandstof1><rdw:brandstof2 xsi:nil=\"true\" /><rdw:kleur1 code=\"07\">grijs</rdw:kleur1><rdw:kleur2 code=\"99\">niet van toepassing</rdw:kleur2><rdw:aantalZitplaatsen>5</rdw:aantalZitplaatsen><rdw:aantalStaanplaatsen>0</rdw:aantalStaanplaatsen><rdw:datumEersteToelatingInternationaal>2010-11-29</rdw:datumEersteToelatingInternationaal><rdw:datumEersteToelatingNationaal>2010-11-29</rdw:datumEersteToelatingNationaal><rdw:datumAansprakelijkheid>2018-03-14</rdw:datumAansprakelijkheid><rdw:datumVervalApk>2019-02-27</rdw:datumVervalApk><rdw:aantalCilinders>4</rdw:aantalCilinders><rdw:cilinderinhoud>1798</rdw:cilinderinhoud><rdw:massaLeegVoertuig>1250</rdw:massaLeegVoertuig><rdw:laadvermogen>0</rdw:laadvermogen><rdw:maximumMassa>1950</rdw:maximumMassa><rdw:massaRijklaar>1350</rdw:massaRijklaar><rdw:maximumMassaOngeremd>650</rdw:maximumMassaOngeremd><rdw:maximumMassaGeremd>1300</rdw:maximumMassaGeremd><rdw:maximumMassaOpleggerGeremd>0</rdw:maximumMassaOpleggerGeremd><rdw:maximumMassaAutonoomGeremd>0</rdw:maximumMassaAutonoomGeremd><rdw:maximumMassaMiddenasGeremd>1300</rdw:maximumMassaMiddenasGeremd><rdw:parallelImport>false</rdw:parallelImport><rdw:uitvoeringsVolgnummer>23526</rdw:uitvoeringsVolgnummer><rdw:aantalDeuren>4</rdw:aantalDeuren><rdw:inrichting code=\"85\">sedan</rdw:inrichting><rdw:voertuigClassificatie code=\"01\">personenauto</rdw:voertuigClassificatie><rdw:aantalWielen>4</rdw:aantalWielen><rdw:vermogenKw>118</rdw:vermogenKw><rdw:vermogenBromfiets xsi:nil=\"true\" /><rdw:maximaleConstructiesnelheid>0</rdw:maximaleConstructiesnelheid><rdw:emissieCode>5</rdw:emissieCode><rdw:g3Installatie>false</rdw:g3Installatie><rdw:bpm>5329</rdw:bpm><rdw:verplichtingennemer>false</rdw:verplichtingennemer><rdw:wamVerzekerd>true</rdw:wamVerzekerd><rdw:wielbasis>258</rdw:wielbasis><rdw:motorcode>CDA</rdw:motorcode><rdw:catalogusPrijs>26982</rdw:catalogusPrijs><rdw:isTaxi>false</rdw:isTaxi><rdw:typeEigenaar>(erkend) bedrijf</rdw:typeEigenaar><rdw:datumMeldApk>2017-02-27T00:00:00</rdw:datumMeldApk><rdw:wijzeVanInvoer>versneld</rdw:wijzeVanInvoer><rdw:isDubbeleCabine>false</rdw:isDubbeleCabine><rdw:tijdAansprakelijkheid>11:34:00</rdw:tijdAansprakelijkheid><rdw:brandstof3 xsi:nil=\"true\" /></rdwInfoAdvanced></rubrieken></antwoordbericht>",
    "SB-RDW-HIST-ADV" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-RDW-HIST-ADV&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040323418</interdataReferentie></resultaat><rubrieken><rdwHistInfoAdvanced xsi:type=\"rdw:rdwHistInfoAdvancedTypeV1\" xmlns:rdw=\"http://www.xmlmode.nl/interdata/rdw\"><rdw:kenteken>61NVL2</rdw:kenteken><rdw:eigenaar><rdw:datumAansprakelijkheidVanaf>2010-11-29</rdw:datumAansprakelijkheidVanaf><rdw:datumAansprakelijkheidTot>2017-07-12</rdw:datumAansprakelijkheidTot><rdw:typeEigenaar>Lease op naam</rdw:typeEigenaar><rdw:aantalDagen>2417</rdw:aantalDagen></rdw:eigenaar><rdw:eigenaar><rdw:datumAansprakelijkheidVanaf>2017-07-12</rdw:datumAansprakelijkheidVanaf><rdw:datumAansprakelijkheidTot>2017-11-09</rdw:datumAansprakelijkheidTot><rdw:typeEigenaar>(erkend) bedrijf</rdw:typeEigenaar><rdw:aantalDagen>120</rdw:aantalDagen></rdw:eigenaar><rdw:eigenaar><rdw:datumAansprakelijkheidVanaf>2017-11-09</rdw:datumAansprakelijkheidVanaf><rdw:datumAansprakelijkheidTot>2018-03-14</rdw:datumAansprakelijkheidTot><rdw:typeEigenaar>Particulier</rdw:typeEigenaar><rdw:aantalDagen>125</rdw:aantalDagen></rdw:eigenaar><rdw:eigenaar><rdw:datumAansprakelijkheidVanaf>2018-03-14</rdw:datumAansprakelijkheidVanaf><rdw:datumAansprakelijkheidTot xsi:nil=\"true\" /><rdw:typeEigenaar>(erkend) bedrijf</rdw:typeEigenaar><rdw:aantalDagen>231</rdw:aantalDagen></rdw:eigenaar></rdwHistInfoAdvanced></rubrieken></antwoordbericht>",
    "SB-RDW-IMPORTLAND" => " <antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-RDW-IMPORTLAND&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>80</code><omschrijving>Geen voertuiggegevens gevonden.</omschrijving><interdataReferentie>1040323428</interdataReferentie></resultaat><rubrieken /></antwoordbericht>",

    "SB-ATD-TCO" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-ATD-TCO&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>94</code><omschrijving>Bericht niet actief.</omschrijving><interdataReferentie>1040409421</interdataReferentie></resultaat><rubrieken /></antwoordbericht>",
    "SB-ATD-TYRE-INFO" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-ATD-TYRE-INFO&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040409422</interdataReferentie></resultaat><rubrieken><atdTyreInfo xsi:type=\"atd:atdTyreInfoTypeV1\" xmlns:atd=\"http://www.xmlmode.nl/interdata/atd\"><atd:kenteken>61NVL2</atd:kenteken><atd:uitvoeringen><atd:uitvoering><atd:typeId>4110853</atd:typeId><atd:bandenmaatvoor>195/65</atd:bandenmaatvoor><atd:bandenmaatachter>195/65</atd:bandenmaatachter><atd:velgmaatinch>15</atd:velgmaatinch></atd:uitvoering><atd:uitvoering><atd:typeId>4110907</atd:typeId><atd:bandenmaatvoor>205/60</atd:bandenmaatvoor><atd:bandenmaatachter>205/60</atd:bandenmaatachter><atd:velgmaatinch>15</atd:velgmaatinch></atd:uitvoering><atd:uitvoering><atd:typeId>4110865</atd:typeId><atd:bandenmaatvoor>205/60</atd:bandenmaatvoor><atd:bandenmaatachter>205/60</atd:bandenmaatachter><atd:velgmaatinch>15</atd:velgmaatinch></atd:uitvoering><atd:uitvoering><atd:typeId>4110908</atd:typeId><atd:bandenmaatvoor>205/55</atd:bandenmaatvoor><atd:bandenmaatachter>205/55</atd:bandenmaatachter><atd:velgmaatinch>16</atd:velgmaatinch></atd:uitvoering><atd:uitvoering><atd:typeId>4110866</atd:typeId><atd:bandenmaatvoor>225/45</atd:bandenmaatvoor><atd:bandenmaatachter>225/45</atd:bandenmaatachter><atd:velgmaatinch>17</atd:velgmaatinch></atd:uitvoering></atd:uitvoeringen></atdTyreInfo></rubrieken></antwoordbericht>",
    "SB-ATL-TAX-ONLINE" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-ATL-TAX-ONLINE&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040409424</interdataReferentie></resultaat><rubrieken><atlTaxatieOnline xsi:type=\"atl:atlTaxatieOnlineTypeV1\" xmlns:atl=\"http://www.xmlmode.nl/interdata/atl\"><atl:kenteken>61NVL2</atl:kenteken><atl:atlCode>90692</atl:atlCode><atl:uitvoering>1.8 TSI Ambition</atl:uitvoering><atl:ranking>1</atl:ranking><atl:merk>Skoda</atl:merk><atl:model>Octavia</atl:model><atl:dagwaardeVerkoop>6567</atl:dagwaardeVerkoop><atl:dagwaardeInruil>5767</atl:dagwaardeInruil><atl:dagwaardeHandel>5367</atl:dagwaardeHandel></atlTaxatieOnline></rubrieken></antwoordbericht>",
    "SB-RDW-HIST-APK" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-RDW-HIST-APK&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040409425</interdataReferentie></resultaat><rubrieken><rdwApkKeuringspunt xsi:type=\"rdw:rdwApkKeuringspuntTypeV2\" xmlns:rdw=\"http://www.xmlmode.nl/interdata/rdw\"><rdw:keuring><rdw:datumtijd>2017-02-27T00:00:00</rdw:datumtijd></rdw:keuring><rdw:keuring><rdw:datumtijd>2014-12-09T00:00:00</rdw:datumtijd><rdw:bevindingen><rdw:bevinding><rdw:volgnr>1</rdw:volgnr><rdw:gebrek_identificatie>G05</rdw:gebrek_identificatie><rdw:gebrek_omschrijving>Band onvoldoende profiel 5.*.27</rdw:gebrek_omschrijving><rdw:soort_actie>REP</rdw:soort_actie><rdw:aantal_gebreken>1</rdw:aantal_gebreken></rdw:bevinding><rdw:bevinding><rdw:volgnr>2</rdw:volgnr><rdw:gebrek_identificatie>K04</rdw:gebrek_identificatie><rdw:gebrek_omschrijving>Werking/toestand verplicht licht/retroreflector 5.*.55</rdw:gebrek_omschrijving><rdw:soort_actie>REP</rdw:soort_actie><rdw:aantal_gebreken>2</rdw:aantal_gebreken></rdw:bevinding></rdw:bevindingen></rdw:keuring></rdwApkKeuringspunt></rubrieken></antwoordbericht>",
    "SB-RDW-OVIZ-REAL" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-RDW-OVIZ-REAL&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>99</code><omschrijving>Applicatiefout opgetreden.</omschrijving><interdataReferentie>1040409428</interdataReferentie></resultaat><rubrieken /></antwoordbericht>",
    "SB-RDW-STATUS-HIST" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-RDW-STATUS-HIST&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>00</code><omschrijving>Ok</omschrijving><interdataReferentie>1040409429</interdataReferentie></resultaat><rubrieken><rdwStatusHistorie xsi:type=\"rdw:rdwStatusHistorieTypeV1\" xmlns:rdw=\"http://www.xmlmode.nl/interdata/rdw\"><rdw:kenteken>61NVL2</rdw:kenteken><rdw:wokstatus>false</rdw:wokstatus><rdw:sloopstatus>false</rdw:sloopstatus><rdw:exportstatus>false</rdw:exportstatus><rdw:geldigstatus>true</rdw:geldigstatus></rdwStatusHistorie></rubrieken></antwoordbericht>",
    "SB-STAT-WP-STA-BEZIT" => "<antwoordbericht xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><aanvraag>&lt;bericht&gt; &lt;authenticatie&gt; &lt;naam&gt;Cell01&lt;/naam&gt; &lt;wachtwoord&gt;nyx66M&lt;/wachtwoord&gt; &lt;berichtsoort&gt;SB-STAT-WP-STA-BEZIT&lt;/berichtsoort&gt; &lt;referentie&gt;&lt;/referentie&gt; &lt;/authenticatie&gt; &lt;parameters&gt; &lt;kenteken&gt;61NVL2&lt;/kenteken&gt; &lt;/parameters&gt; &lt;/bericht&gt;</aanvraag><resultaat><code>90</code><omschrijving>Ongeldige naam of wachtwoord.</omschrijving></resultaat></antwoordbericht>",

];*/


$params = [];

//SB-ATL-MMT
$param['kenteken'] = findParameter($parsed['SB-ATL-MMT'], '<atl:kenteken>');
$params['merk'] = findParameter($parsed['SB-ATL-MMT'], '<atl:merk>');
$params['model'] = findParameter($parsed['SB-ATL-MMT'], '<atl:model>');
$params['atlCode'] = findParameter($parsed['SB-ATL-MMT'], '<atl:atlCode>');
$params['uitvoering'] = findParameter($parsed['SB-ATL-MMT'], '<atl:uitvoering>');
$params['uitvoering_lang'] = findParameter($parsed['SB-ATL-MMT'], '<atl:uitvoering_lang>');
$params['fabrieksCode'] = findParameter($parsed['SB-ATL-MMT'], '<atl:fabrieksCode>');

//SB-ATL-OPTIE-FABRIEK
$param['kenteken'] = findParameter($parsed['SB-ATL-OPTIE-FABRIEK'], '<atl:kenteken>');
$params['atlCode'] = findParameter($parsed['SB-ATL-OPTIE-FABRIEK'], '<atl:atlCode>');
$params['fabriek-opties'] = findParameterListWithAttributes($parsed['SB-ATL-OPTIE-FABRIEK'], '<atl:optie');

//SB-ATL-PP
$params['gewicht'] = findParameter($parsed['SB-ATL-PP'], '<atl:gewicht>');
$params['treinGewicht'] = findParameter($parsed['SB-ATL-PP'], '<atl:treinGewicht>');
$params['aandrijving'] = findParameterWithAttributes($parsed['SB-ATL-PP'], '<atl:aandrijving');
$params['carrosserieType'] = findParameter($parsed['SB-ATL-PP'], '<atl:carrosserieType>');
$params['aantalDeuren'] = findParameter($parsed['SB-ATL-PP'], '<atl:aantalDeuren>');
$params['acceleratie'] = findParameter($parsed['SB-ATL-PP'], '<atl:acceleratie>');
$params['sportiviteitsKlasse'] = findParameter($parsed['SB-ATL-PP'], '<atl:sportiviteitsKlasse>');
$params['topsnelheid'] = findParameter($parsed['SB-ATL-PP'], '<atl:topsnelheid>');
$params['emissieCode'] = findParameter($parsed['SB-ATL-PP'], '<atl:emissieCode>');
$params['energielabel'] = findParameter($parsed['SB-ATL-PP'], '<atl:energielabel>');
$params['vermogenKw'] = findParameter($parsed['SB-ATL-PP'], '<atl:vermogenKw>');
$params['cilinderinhoud'] = findParameter($parsed['SB-ATL-PP'], '<atl:cilinderinhoud>');
$params['brandstof'] = findParameterWithAttributes($parsed['SB-ATL-PP'], '<atl:brandstof');
$params['versnellingsType'] = findParameter($parsed['SB-ATL-PP'], '<atl:versnellingsType>');
$params['aantalZitplaatsen'] = findParameter($parsed['SB-ATL-PP'], '<atl:aantalZitplaatsen>');

//SB-MILIEU-BASIC
$params['kenteken'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:kenteken>');
$params['roetfilter'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:roetfilter>');
$params['roetdeeltjes'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:roetdeeltjes>');
$params['co2uitstootGecombineerd'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:co2uitstootGecombineerd>');
$params['euroklasse'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:euroklasse>');
$params['energielabel'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:energielabel>');
$params['brandstofverbruikStad'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:brandstofverbruikStad>');
$params['brandstofverbruikBuitenweg'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:brandstofverbruikBuitenweg>');
$params['brandstofverbruikGecombineerd'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:brandstofverbruikGecombineerd>');
$params['typeVersnellingsbak'] = findParameterWithAttributes($parsed['SB-MILIEU-BASIC'], '<mil:typeVersnellingsbak');
$params['aantalVersnellingen'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:aantalVersnellingen>');
$params['bijtellingsKlasse'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:bijtellingsKlasse>');
$params['coEmissie'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:coEmissie>');
$params['hcEmissie'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:hcEmissie>');
$params['noxEmissie'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:noxEmissie>');
$params['hcNoxEmissie'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:hcNoxEmissie>');
//todo:hcNoxEmissie
$params['geluidsniveauStilstaand'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:geluidsniveauStilstaand>');
$params['toerenGeluidsniveauStilstaand'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:toerenGeluidsniveauStilstaand>');
$params['geluidsniveauRijdend'] = findParameter($parsed['SB-MILIEU-BASIC'], '<mil:geluidsniveauRijdend>');
//todo: geluidsniveauRijdend

//SB-MRB
//todo
$params['provincie'] = findParameterList($parsed['SB-MRB'], '<mrb:provincie>');
$params['bedragKwartaal'] = findParameterList($parsed['SB-MRB'], '<mrb:bedragKwartaal>');

//SB-OKR-CHECK
//todo: no data
//$params['code'] = findParameter($parsed['SB-OKR-CHECK'], '<atl:code>');
//$params['omschrijving'] = findParameter($parsed['SB-OKR-CHECK'], '<omschrijving>');
//$params['interdataReferentie'] = findParameter($parsed['SB-OKR-CHECK'], '<interdataReferentie>');

//SB-RDW-ADVANCED
$params['kenteken'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:kenteken>');
$params['isMeldCodeCorrect'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:isMeldCodeCorrect>');
$params['kentekenSignaal'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:kentekenSignaal>');
//todo: kentekenSignaal
$params['handelsbenaming'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:handelsbenaming>');
$params['voertuigsoort'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:voertuigsoort>');
$params['brandstof1'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:brandstof1>');//todo
$params['brandstof2'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:brandstof2>');//todo
//todo: brandstof2
$params['kleur1'] = findParameterWithAttributes($parsed['SB-RDW-ADVANCED'], '<rdw:kleur1');
$params['kleur2'] = findParameterWithAttributes($parsed['SB-RDW-ADVANCED'], '<rdw:kleur2');
$params['aantalZitplaatsen'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:aantalZitplaatsen>');
$params['aantalStaanplaatsen'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:aantalStaanplaatsen>');
$params['datumEersteToelatingInternationaal'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:datumEersteToelatingInternationaal>');
$params['datumEersteToelatingNationaal'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:datumEersteToelatingNationaal>');
$params['datumAansprakelijkheid'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:datumAansprakelijkheid>');
$params['datumVervalApk'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:datumVervalApk>');
$params['aantalCilinders'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:aantalCilinders>');
$params['cilinderinhoud'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:cilinderinhoud>');
$params['massaLeegVoertuig'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:massaLeegVoertuig>');
$params['laadvermogen'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:laadvermogen>');
$params['maximumMassa'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximumMassa>');
$params['massaRijklaar'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:massaRijklaar>');
$params['maximumMassaOngeremd'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximumMassaOngeremd>');
$params['maximumMassaGeremd'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximumMassaGeremd>');
$params['maximumMassaOpleggerGeremd'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximumMassaOpleggerGeremd>');
$params['maximumMassaAutonoomGeremd'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximumMassaAutonoomGeremd>');
$params['maximumMassaMiddenasGeremd'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximumMassaMiddenasGeremd>');
$params['parallelImport'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:parallelImport>');
$params['uitvoeringsVolgnummer'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:uitvoeringsVolgnummer>');
$params['aantalDeuren'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:aantalDeuren>');
$params['inrichting'] = findParameterWithAttributes($parsed['SB-RDW-ADVANCED'], '<rdw:inrichting');
$params['voertuigClassificatie'] = findParameterWithAttributes($parsed['SB-RDW-ADVANCED'], '<rdw:voertuigClassificatie');
$params['aantalWielen'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:aantalWielen>');
$params['vermogenKw'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:vermogenKw>');
$params['vermogenBromfiets'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:vermogenBromfiets>');
//todo: vermogenBromfiets
$params['maximaleConstructiesnelheid'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:maximaleConstructiesnelheid>');
$params['emissieCode'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:emissieCode>');
$params['g3Installatie'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:g3Installatie>');
$params['bpm'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:bpm>');
$params['verplichtingennemer'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:verplichtingennemer>');
$params['wamVerzekerd'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:wamVerzekerd>');
$params['wielbasis'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:wielbasis>');
$params['motorcode'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:motorcode>');
$params['catalogusPrijs'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:catalogusPrijs>');
$params['isTaxi'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:isTaxi>');
$params['typeEigenaar'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:typeEigenaar>');
$params['datumMeldApk'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:datumMeldApk>');
$params['wijzeVanInvoer'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:wijzeVanInvoer>');
$params['isDubbeleCabine'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:isDubbeleCabine>');
$params['tijdAansprakelijkheid'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:tijdAansprakelijkheid>');
$params['brandstof3'] = findParameter($parsed['SB-RDW-ADVANCED'], '<rdw:brandstof3>');
//todo: brandstof3

//SB-RDW-HIST-ADV
$params['kenteken'] = findParameter($parsed['SB-RDW-HIST-ADV'], '<rdw:kenteken>');
$params['datumAansprakelijkheidVanaf'] = findParameterList($parsed['SB-RDW-HIST-ADV'], '<rdw:datumAansprakelijkheidVanaf>');
$params['datumAansprakelijkheidTot'] = findParameterList($parsed['SB-RDW-HIST-ADV'], '<rdw:datumAansprakelijkheidTot>');
//$params['typeEigenaar'] = findParameterList($parsed['SB-RDW-HIST-ADV'], '<rdw:typeEigenaar>');
$params['aantalDagen'] = findParameterList($parsed['SB-RDW-HIST-ADV'], '<rdw:aantalDagen>');

//SB-RDW-IMPORTLAND
//todo: no data
$params['importauto'] = findParameter($parsed['SB-RDW-IMPORTLAND'], '<code>') == '00' ? 'Yes': 'No';
$params['importland'] = findParameter($parsed['SB-RDW-IMPORTLAND'], '<landHerkomst>');

//SB-ATD-TCO
//todo: no data

//SB-ATD-TYRE-INFO
$params['typeId'] = findParameterList($parsed['SB-ATD-TYRE-INFO'], '<atd:typeId>');
$params['bandenmaatvoor'] = findParameterList($parsed['SB-ATD-TYRE-INFO'], '<atd:bandenmaatvoor>');
$params['bandenmaatachter'] = findParameterList($parsed['SB-ATD-TYRE-INFO'], '<atd:bandenmaatachter>');
$params['velgmaatinch'] = findParameterList($parsed['SB-ATD-TYRE-INFO'], '<atd:velgmaatinch>');

//SB-ATL-TAX-ONLINE
$params['ranking'] = findParameter($parsed['SB-ATL-TAX-ONLINE'], '<atl:ranking>');
$params['dagwaardeVerkoop'] = findParameter($parsed['SB-ATL-TAX-ONLINE'], '<atl:dagwaardeVerkoop>');
$params['dagwaardeInruil'] = findParameter($parsed['SB-ATL-TAX-ONLINE'], '<atl:dagwaardeInruil>');
$params['dagwaardeHandel'] = findParameter($parsed['SB-ATL-TAX-ONLINE'], '<atl:dagwaardeHandel>');

//SB-RDW-HIST-APK
$params['datumtijd'] = findParameterList($parsed['SB-RDW-HIST-APK'], '<rdw:datumtijd>');
$params['volgnr'] = findParameterList($parsed['SB-RDW-HIST-APK'], '<rdw:volgnr>');
$params['gebrek_identificatie'] = findParameterList($parsed['SB-RDW-HIST-APK'], '<rdw:gebrek_identificatie>');
$params['gebrek_omschrijving'] = findParameterList($parsed['SB-RDW-HIST-APK'], '<rdw:gebrek_omschrijving>');
$params['soort_actie'] = findParameterList($parsed['SB-RDW-HIST-APK'], '<rdw:soort_actie>');
$params['aantal_gebreken'] = findParameterList($parsed['SB-RDW-HIST-APK'], '<rdw:aantal_gebreken>');

//SB-RDW-OVIZ-REAL
//todo: no data

//SB-RDW-STATUS-HIST
$params['wokstatus'] = findParameter($parsed['SB-RDW-STATUS-HIST'], '<rdw:wokstatus>');
$params['sloopstatus'] = findParameter($parsed['SB-RDW-STATUS-HIST'], '<rdw:sloopstatus>');
$params['exportstatus'] = findParameter($parsed['SB-RDW-STATUS-HIST'], '<rdw:exportstatus>');
$params['geldigstatus'] = findParameter($parsed['SB-RDW-STATUS-HIST'], '<rdw:geldigstatus>');

//SB-STAT-WP-STA-BEZIT
//todo: no data

if($params['kenteken'] == '') {
    echo 'There is no data for such plate number';
    die();
}

$carNumber = $param['kenteken'];

$damages = [
    'front' => [],
    'back' => [],
    'right' => [],
    'left' => [],
    'date' => '',
    'price' => '',
    'mileage' => ''
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_URL,"http://85.214.132.37:8000/schadeautos/api?plate=76jvd2");
//curl_setopt($ch, CURLOPT_URL,"http://85.214.132.37:8000/schadeautos/api?plate=v030gr");
curl_setopt($ch, CURLOPT_URL,"http://85.214.132.37:8000/schadeautos/api?plate=$carNumber"); //todo: replace by it
$schade=curl_exec($ch);
curl_close($ch);

$schadeDecoded = json_decode($schade, true);
if (array_key_exists('data', $schadeDecoded) && array_key_exists(0, $schadeDecoded['data'])) {
    $damages = $schadeDecoded['data'][0];
}
$damages['parameters'] = array_merge($damages['front'], $damages['back'], $damages['right'], $damages['left']);


$damagePhotos = [
    'front_left' => 'Fron_Left.jpg',
    'front_middle' => 'Front_Mid.jpg',
    'front_right' => 'Front Right.jpg',
    'front' => 'Front_Full.jpg',
    'back_left' => 'Back_Left Right.jpg',
    'back_middle' => 'Back_Mid.jpg',
    'back_right' => 'Back_Mid Right.jpg',
    'back' => 'Back_Full.jpg',
    'right_front' => 'Right_Front copy.jpg',
    'right_middle' => 'Right_Mid copy.jpg',
    'right_back' => 'Right_Back copy.jpg',
    'right' => 'Right_Full copy.jpg',
    'left_front' => 'Left_Front.jpg',
    'left_middle' => 'Left_Mid.jpg',
    'left_back' => 'Left_Back.jpg',
    'left' => 'LEFT Side.png',
    'roof' => 'TOP_Roof.jpg',

    'top_front_left' => 'TOP_Front Left.jpg',
    'top_front_middle' => 'TOP_Front Mid.jpg',
    'top_front_right' => 'TOP_Front Right.jpg',
    'top_front' => 'TOP_Front Full.jpg',
    'top_back_left' => 'TOP_Back Left.jpg',
    'top_back_middle' => 'TOP_Back Mid.jpg',
    'top_back_right' => 'TOP_Back Right.jpg',
    'top_back' => 'TOP_Back FULL.jpg',
    'top_right_front' => 'TOP_Right Front.jpg',
    'top_right_middle' => 'TOP_Right Middle.jpg',
    'top_right_back' => 'TOP_Right Back.jpg',
    'top_right' => 'TOP_Right Full.jpg',
    'top_left_front' => 'TOP_Left Front.jpg',
    'top_left_middle' => 'TOP_Left Middle.jpg',
    'top_left_back' => 'TOP_Left Back.jpg',
    'top_left' => 'TOP_Left Full.png',
];


$mp_images_folder = '';
$mp_data = [];

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,"http://85.214.132.37:8000/marktplaats/api?plate=$carNumber");
$schade=curl_exec($ch);
curl_close($ch);

$marktplaats = json_decode($schade, true);
if (array_key_exists('images_folder', $marktplaats)) {
    $mp_images_folder = $marktplaats['images_folder'];
}
if (array_key_exists('data', $marktplaats) && array_key_exists(0, $marktplaats['data'])) {
    $mp_data = $marktplaats['data'][0];
}

?>


<img src="{{ url('/')."/storage/images/logo.png" }}" class="image-logo" alt="site logo" />

<div class="plate-number-container">
    <img src="{{ url('/')."/storage/images/plate.png" }}" alt="plate number" />
    <div class="plate-number-text"><?=strtoupper($param['kenteken'])?></div>
</div>
<div class="reportdatum">reportdatum: <?=date("d-m-Y")?></div>

<h3>General vehicle information</h3>
<h4>Characteristics</h4>
<p><b><?=strtoupper($params['merk'])?> <?=strtoupper($params['model'])?></b>, uit <?=strtolower(date("F Y", strtotime($params['datumEersteToelatingInternationaal'])))?> Dit voertuig had destijds een nieuwprijs vanaf <b>€<?=$params['catalogusPrijs']?></b>,- en is een origineel Nederlandse auto.</p>
<p>De auto kan van 0-100km/h in <b><?=1.0 * $params['acceleratie'] / 10?> seconden</b> en heeft een topsnelheid van <b><?=$params['topsnelheid']?> km/h</b> en een maximum vermogen van <b><?=ceil(1.3596 * $params['vermogenKw'])?> PK</b>.</p>
<p>Deze OCTAVIA verbruikt gemiddeld <b>1 liter op de <?=round(100/$params['brandstofverbruikGecombineerd'], 1)?> kilometer</b>. Met een <b>CO2-uitstoot van <?=$params['co2uitstootGecombineerd']?> gram</b> per kilometer krijgt deze SKODA een <b>zuinigheidslabel <?=$params['energielabel']?></b></p>

<h4>WARNINGS</h4>
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Used to be a taxi</td>
        <td><?=$params['isTaxi']?></td>
    </tr>
    <tr>
        <td>Had a big accident (wok)</td>
        <td><?=$params['wokstatus']?></td>
    </tr>
    <tr>
        <td>Is the car in our scraped database</td>
        <td><?=empty($mp_data)? 'Yes': 'No'?></td>
    </tr>
    <tr>
        <td>Imported from another country</td>
        <td><?=$params['importauto']?></td>
    </tr>
    <tr>
        <td>Had a lot of previous owners</td>
        <td><?=count($params['datumAansprakelijkheidVanaf']) > 10 ? 'Yes': 'No'?></td>
    </tr>
    <tr>
        <td>Was stolen</td>
        <td>No</td>
    </tr>
    <tr>
        <td>Is the car price higher then it was previosly sold</td>
        <td>No</td>
    </tr>
</table>

<div class="page_break"></div>

{{--<h1>Algemene informatie</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Merk</td>
        <td><?=strtoupper($params['merk'])?></td>
    </tr>
    <tr>
        <td>Type</td>
        <td><?=strtoupper($params['model'])?></td>
    </tr>
    <tr>
        <td>Kleur</td>
        <td><?=strtoupper($params['kleur1'])?></td>
    </tr>
    <tr>
        <td>Bouwjaar</td>
        <td><?=$params['datumEersteToelatingInternationaal']?></td> <!--datumEersteToelatingInternationaal or datumEersteToelatingNationaal-->
    </tr>
    <tr>
        <td>Nieuwprijs</td>
        <td><?=$params['catalogusPrijs']?> &euro;</td>
    </tr>
    <tr>
        <td>Brandstof</td>
        <td><?=$params['brandstof']?></td>
    </tr>
    <tr>
        <td>Vermogen</td>
        <td><?=$params['vermogenKw']?> kW / <?=ceil($params['vermogenKw'] * 1.3596)?> PK</td>
    </tr>
    <tr>
        <td>0-100km/h</td>
        <td><?=$params['acceleratie'] / 10?> seconden</td>
    </tr>
    <tr>
        <td>Topsnelheid</td>
        <td><?=$params['topsnelheid']?></td>
    </tr>
    <tr>
        <td>Transmissie</td>
        <td><?=$params['aantalVersnellingen']?> v., <?=$params['typeVersnellingsbak']?></td>
    </tr>
    <tr>
        <td>Aantal eigenaren</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Importauto</td>
        <td><?=$params['importauto']?></td>
    </tr>
    <tr>
        <td>Import land</td>
        <td><?=$params['importland']?></td>
    </tr>
    <tr>
        <td>APK tot</td>
        <td><?=$params['datumVervalApk']?></td>
    </tr>
</table>

{{--<h1>Motor en milieu</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 30%"/>
        <col style="width: 40%"/>
    </colgroup>
    <tr>
        <td>Vermogen</td>
        <td><?=$params['vermogenKw']?> kW / <?=ceil($params['vermogenKw'] * 1.3596)?> PK</td>
        <td rowspan="8"><img class="energy-label-picture" src="{{url('/') . '/storage/images/energy_labels/Energy-Label-' . $params['energielabel'] . '.png'}}" alt="no-label"/></td>
    </tr>
    <tr>
        <td>Koppel</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Aantal cilinders</td>
        <td><?=$params['aantalCilinders']?></td>
    </tr>
    <tr>
        <td>Cilinderinhoud</td>
        <td><?=$params['cilinderinhoud']?>cc</td>
    </tr>
    <tr>
        <td>Verbruik stad</td>
        <td><?=$params['brandstofverbruikStad']?> l/100km (1:<?=round(100/$params['brandstofverbruikStad'], 1)?>km)</td>
    </tr>
    <tr>
        <td>Verbruik snelweg</td>
        <td><?=$params['brandstofverbruikBuitenweg']?> l/100km (1:<?=round(100/$params['brandstofverbruikBuitenweg'], 1)?>km)</td>
    </tr>
    <tr>
        <td>CO2 uitstoot</td>
        <td><?=$params['co2uitstootGecombineerd']?> g/km</td>
    </tr>
    <tr>
        <td>Zuinigheidslabel</td>
        <td><?=$params['energielabel']?></td>
    </tr>
</table>

{{--<h1>MMT</h1>--}}
<table class="formatted-table formatted-table-size">
    <tr>
        <td>atlCode</td>
        <td><?=$params['atlCode']?></td>
    </tr>
    <tr>
        <td>uitvoering</td>
        <td><?=$params['uitvoering']?></td>
    </tr>
    <tr>
        <td>uitvoering_lang</td>
        <td><?=$params['uitvoering_lang']?></td>
    </tr>
    <tr>
        <td>fabrieksCode</td>
        <td><?=$params['fabrieksCode']?></td>
    </tr>
</table>

<div class="page_break"></div>

{{--<h1>ADVANCED</h1>--}}
<table class="formatted-table formatted-table-size">
    <tr>
        <td>isMeldCodeCorrect</td>
        <td><?=$params['isMeldCodeCorrect']?></td>
    </tr>
    <tr>
        <td>kentekenSignaal</td>
        <td><?=$params['kentekenSignaal']?></td>
    </tr>
    <tr>
        <td>voertuigsoort</td>
        <td><?=$params['voertuigsoort']?></td>
    </tr>
    <tr>
        <td>brandstof1</td>
        <td><?=$params['brandstof1']?></td>
    </tr>
    <tr>
        <td>brandstof2</td>
        <td><?=$params['brandstof2']?></td>
    </tr>
    <tr>
        <td>kleur1</td>
        <td><?=$params['kleur1']?></td>
    </tr>
    <tr>
        <td>kleur2</td>
        <td><?=$params['kleur2']?></td>
    </tr>
    <tr>
        <td>aantalZitplaatsen</td>
        <td><?=$params['aantalZitplaatsen']?></td>
    </tr>
    <tr>
        <td>aantalStaanplaatsen</td>
        <td><?=$params['aantalStaanplaatsen']?></td>
    </tr>
    <tr>
        <td>datumEersteToelatingInternationaal</td>
        <td><?=$params['datumEersteToelatingInternationaal']?></td>
    </tr>
    <tr>
        <td>datumEersteToelatingNationaal</td>
        <td><?=$params['datumEersteToelatingNationaal']?></td>
    </tr>
    <tr>
        <td>datumAansprakelijkheid</td>
        <td><?=$params['datumAansprakelijkheid']?></td>
    </tr>
    <tr>
        <td>datumVervalApk</td>
        <td><?=$params['datumVervalApk']?></td>
    </tr>
    <tr>
        <td>aantalCilinders</td>
        <td><?=$params['aantalCilinders']?></td>
    </tr>
    <tr>
        <td>cilinderinhoud</td>
        <td><?=$params['cilinderinhoud']?></td>
    </tr>
    <tr>
        <td>massaLeegVoertuig</td>
        <td><?=$params['massaLeegVoertuig']?></td>
    </tr>
    <tr>
        <td>laadvermogen</td>
        <td><?=$params['laadvermogen']?></td>
    </tr>
    <tr>
        <td>maximumMassa</td>
        <td><?=$params['maximumMassa']?></td>
    </tr>
    <tr>
        <td>massaRijklaar</td>
        <td><?=$params['massaRijklaar']?></td>
    </tr>
    <tr>
        <td>maximumMassaOngeremd</td>
        <td><?=$params['maximumMassaOngeremd']?></td>
    </tr>
    <tr>
        <td>maximumMassaGeremd</td>
        <td><?=$params['maximumMassaGeremd']?></td>
    </tr>
    <tr>
        <td>maximumMassaOpleggerGeremd</td>
        <td><?=$params['maximumMassaOpleggerGeremd']?></td>
    </tr>
    <tr>
        <td>parallelImport</td>
        <td><?=$params['parallelImport']?></td>
    </tr>
    <tr>
        <td>uitvoeringsVolgnummer</td>
        <td><?=$params['uitvoeringsVolgnummer']?></td>
    </tr>
    <tr>
        <td>aantalDeuren</td>
        <td><?=$params['aantalDeuren']?></td>
    </tr>
    <tr>
        <td>inrichting</td>
        <td><?=$params['inrichting']?></td>
    </tr>
    <tr>
        <td>voertuigClassificatie</td>
        <td><?=$params['voertuigClassificatie']?></td>
    </tr>
    <tr>
        <td>aantalWielen</td>
        <td><?=$params['aantalWielen']?></td>
    </tr>
    <tr>
        <td>vermogenKw</td>
        <td><?=$params['vermogenKw']?></td>
    </tr>
    <tr>
        <td>vermogenBromfiets</td>
        <td><?=$params['vermogenBromfiets']?></td>
    </tr>
    <tr>
        <td>maximaleConstructiesnelheid</td>
        <td><?=$params['maximaleConstructiesnelheid']?></td>
    </tr>
    <tr>
        <td>emissieCode</td>
        <td><?=$params['emissieCode']?></td>
    </tr>
    <tr>
        <td>g3Installatie</td>
        <td><?=$params['g3Installatie']?></td>
    </tr>
    <tr>
        <td>bpm</td>
        <td><?=$params['bpm']?></td>
    </tr>
    <tr>
        <td>verplichtingennemer</td>
        <td><?=$params['verplichtingennemer']?></td>
    </tr>
    <tr>
        <td>wamVerzekerd</td>
        <td><?=$params['wamVerzekerd']?></td>
    </tr>
    <tr>
        <td>wielbasis</td>
        <td><?=$params['wielbasis']?></td>
    </tr>
    <tr>
        <td>motorcode</td>
        <td><?=$params['motorcode']?></td>
    </tr>
    <tr>
        <td>catalogusPrijs</td>
        <td><?=$params['catalogusPrijs']?></td>
    </tr>
    <tr>
        <td>isTaxi</td>
        <td><?=$params['isTaxi']?></td>
    </tr>
    <tr>
        <td>typeEigenaar</td>
        <td><?=$params['typeEigenaar']?></td>
    </tr>
    <tr>
        <td>datumMeldApk</td>
        <td><?=$params['datumMeldApk']?></td>
    </tr>
    <tr>
        <td>wijzeVanInvoer</td>
        <td><?=$params['wijzeVanInvoer']?></td>
    </tr>
    <tr>
        <td>isDubbeleCabine</td>
        <td><?=$params['isDubbeleCabine']?></td>
    </tr>
    <tr>
        <td>tijdAansprakelijkheid</td>
        <td><?=$params['tijdAansprakelijkheid']?></td>
    </tr>
    <tr>
        <td>brandstof3</td>
        <td><?=$params['brandstof3']?></td>
    </tr>
</table>

<div class="page_break"></div>

{{--<h1>Maten & gewichten</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Gewicht</td>
        <td><?=$params['gewicht']?> Kg</td>
    </tr>
    <tr>
        <td>Trein Gewicht</td>
        <td><?=$params['treinGewicht'] ? $params['treinGewicht'] : 0?> Kg</td>
    </tr>
    <tr>
        <td>Aandrijving</td>
        <td><?=$params['aandrijving']?></td>
    </tr>
    <tr>
        <td>Carrosserie Type</td>
        <td><?=$params['carrosserieType']?></td>
    </tr>
    <tr>
        <td>Aantal Deuren</td>
        <td><?=$params['aantalDeuren']?></td>
    </tr>
    <tr>
        <td>Acceleratie</td>
        <td><?=$params['acceleratie']?> milliseconde</td>
    </tr>
    <tr>
        <td>Sportiviteits Klasse</td>
        <td><?=$params['sportiviteitsKlasse']?></td>
    </tr>
    <tr>
        <td>Topsnelheid</td>
        <td><?=$params['topsnelheid']?> Km per uur</td>
    </tr>
    <tr>
        <td>Emissie Code</td>
        <td><?=$params['emissieCode']?></td>
    </tr>
    <tr>
        <td>Energielabel</td>
        <td><?=$params['energielabel']?></td>
    </tr>
    <tr>
        <td>Cilinderinhoud</td>
        <td><?=$params['cilinderinhoud']?> Kubieke Cm</td>
    </tr>
    <tr>
        <td>Brandstof</td>
        <td><?=$params['brandstof']?></td>
    </tr>
    <tr>
        <td>Versnellings Type</td>
        <td><?=$params['versnellingsType']?></td>
    </tr>
    <tr>
        <td>Aantal Zitplaatsen</td>
        <td><?=$params['aantalZitplaatsen']?></td>
    </tr>
</table>

{{--<h1>Overige informatie</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Uitvoering</td>
        <td>***</td>
    </tr>
    <tr>
        <td>BPM bedrag</td>
        <td><?=$params['bpm']?> &euro;</td>
    </tr>
    <tr>
        <td>Handelsbenaming</td>
        <td><?=$params['handelsbenaming']?></td>
    </tr>
    <tr>
        <td>Variant</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Carrosserie</td>
        <td><?=$params['inrichting']?></td>
    </tr>
    <tr>
        <td>Plaats chassisnummer</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Voertuigcategorie</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Boring x slag</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Compressieverhouding</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Eurolabel</td>
        <td><?=$params['euroklasse']?></td>
    </tr>
    <tr>
        <td>Geluid rijdend</td>
        <td>0 dB(A)</td>
    </tr>
    <tr>
        <td>Geluid stationair</td>
        <td><?=$params['geluidsniveauStilstaand']?> dB(A)</td>
    </tr>
    <tr>
        <td>Kleppenbediening</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Tankinhoud</td>
        <td>***</td>
    </tr>
</table>

<div class="page_break"></div>

<h4>Engine & environment</h4>
{{--<h1>MILIEU BASIC</h1>--}}
<table class="formatted-table formatted-table-size">
    <tr>
        <td>roetfilter</td>
        <td><?=$params['roetfilter']?></td>
    </tr>
    <tr>
        <td>roetdeeltjes</td>
        <td><?=$params['roetdeeltjes']?></td>
    </tr>
    <tr>
        <td>co2uitstootGecombineerd</td>
        <td><?=$params['co2uitstootGecombineerd']?></td>
    </tr>
    <tr>
        <td>euroklasse</td>
        <td><?=$params['euroklasse']?></td>
    </tr>
    <tr>
        <td>energielabel</td>
        <td><?=$params['energielabel']?></td>
    </tr>
    <tr>
        <td>brandstofverbruikStad</td>
        <td><?=$params['brandstofverbruikStad']?></td>
    </tr>
    <tr>
        <td>brandstofverbruikBuitenweg</td>
        <td><?=$params['brandstofverbruikBuitenweg']?></td>
    </tr>
    <tr>
        <td>brandstofverbruikGecombineerd</td>
        <td><?=$params['brandstofverbruikGecombineerd']?></td>
    </tr>
    <tr>
        <td>typeVersnellingsbak</td>
        <td><?=$params['typeVersnellingsbak']?></td>
    </tr>
    <tr>
        <td>aantalVersnellingen</td>
        <td><?=$params['aantalVersnellingen']?></td>
    </tr>
    <tr>
        <td>topsnelheid</td>
        <td><?=$params['topsnelheid']?></td>
    </tr>
    <tr>
        <td>bijtellingsKlasse</td>
        <td><?=$params['bijtellingsKlasse']?></td>
    </tr>
    <tr>
        <td>coEmissie</td>
        <td><?=$params['coEmissie']?></td>
    </tr>
    <tr>
        <td>hcEmissie</td>
        <td><?=$params['hcEmissie']?></td>
    </tr>
    <tr>
        <td>noxEmissie</td>
        <td><?=$params['noxEmissie']?></td>
    </tr>
    <tr>
        <td>hcNoxEmissie</td>
        <td><?=$params['hcNoxEmissie']?></td>
    </tr>
    <tr>
        <td>geluidsniveauStilstaand</td>
        <td><?=$params['geluidsniveauStilstaand']?></td>
    </tr>
    <tr>
        <td>toerenGeluidsniveauStilstaand</td>
        <td><?=$params['toerenGeluidsniveauStilstaand']?></td>
    </tr>
    <tr>
        <td>geluidsniveauRijdend</td>
        <td><?=$params['geluidsniveauRijdend']?></td>
    </tr>
</table>


<h4>Performance</h4>
{{--<h1>Prestaties</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Vermogen</td>
        <td><?=$params['vermogenKw']?> kW / <?=ceil($params['vermogenKw'] * 1.3596)?> PK</td>
    </tr>
    <tr>
        <td>Koppel</td>
        <td>***</td>
    </tr>
    <tr>
        <td>Topsnelheid</td>
        <td><?=$params['topsnelheid']?> km/h</td>
    </tr>
    <tr>
        <td>0-100km/h</td>
        <td><?=$params['acceleratie'] / 10?> s</td>
    </tr>
</table>

<div class="page_break"></div>

<h4>Original Features</h4>
{{--<h1>Features</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <?php for($i = 1; $i < count($params['fabriek-opties']); $i++): ?>
    <tr>
        <td>
            <?=$i?>
        </td>
        <td>
            <?=$params['fabriek-opties'][$i]?>
        </td>
    </tr>
    <?php endfor; ?>
</table>

<div class="page_break"></div>

<h4>Tire information</h4>
{{--<h1>Bandenmaten</h1>--}}
<table class="bandenmaten-table">
    <?php for($i = 0; $i < count($params['bandenmaatvoor']); $i++): ?>
    <tr>
        <td>Voorbanden</td>
        <td></td>
        <td><b><?=$params['bandenmaatvoor'][$i]?>R<?=$params['velgmaatinch'][$i]?></b></td>
    </tr>
    <tr>
        <td><?=explode('/', $params['bandenmaatvoor'][$i])[0]?></td>
        <td><?=explode('/', $params['bandenmaatvoor'][$i])[1]?></td>
        <td>R<?=$params['velgmaatinch'][$i]?></td>
    </tr>
    <tr>
        <td><img class="bandenmaten-pic" src="{{ url('/') . '/storage/images/bandenmaten/pic1.png' }}"></td>
        <td><img class="bandenmaten-pic" src="{{ url('/') . '/storage/images/bandenmaten/pic2.png' }}"></td>
        <td><img class="bandenmaten-pic" src="{{ url('/') . '/storage/images/bandenmaten/pic3.png' }}"></td>
    </tr>
    <?endfor;?>
</table>

<table class="bandenmaten-table">
    <?php for($i = 0; $i < count($params['bandenmaatachter']); $i++): ?>
    <tr>
        <td>Achterbanden</td>
        <td></td>
        <td><b><?=$params['bandenmaatachter'][$i]?>R<?=$params['velgmaatinch'][$i]?></b></td>
    </tr>
    <tr>
        <td><?=explode('/', $params['bandenmaatachter'][$i])[0]?></td>
        <td><?=explode('/', $params['bandenmaatachter'][$i])[1]?></td>
        <td>R<?=$params['velgmaatinch'][$i]?></td>
    </tr>
    <tr>
        <td><img class="bandenmaten-pic" src="{{ url('/') . '/storage/images/bandenmaten/pic1.png' }}"></td>
        <td><img class="bandenmaten-pic" src="{{ url('/') . '/storage/images/bandenmaten/pic2.png' }}"></td>
        <td><img class="bandenmaten-pic" src="{{ url('/') . '/storage/images/bandenmaten/pic3.png' }}"></td>
    </tr>
    <?php endfor; ?>
</table>

<br/>

<p>Dit voertuig is vanuit de fabriek uitgevoerd met:</p>
<p>Voorband <b><?php for($i = 0; $i < count($params['bandenmaatvoor']); $i++): ?><?=$params['bandenmaatvoor'][$i]?>R<?=$params['velgmaatinch'][$i]?><?php if($i<count($params['bandenmaatvoor'])-1) echo ', '; ?><?endfor;?></b></p>
<p>Achterband <b><?php for($i = 0; $i < count($params['bandenmaatachter']); $i++): ?><?=$params['bandenmaatachter'][$i]?>R<?=$params['velgmaatinch'][$i]?><?php if($i<count($params['bandenmaatachter'])-1) echo ', '; ?><?endfor;?></b></p>
<p>Het eerste getal (<b><?php for($i = 0; $i < count($params['bandenmaatvoor']); $i++): ?><?=explode('/', $params['bandenmaatvoor'][$i])[0]?><?php if($i<count($params['bandenmaatvoor'])-1) echo ', '; ?><?endfor;?></b>) geeft de breedte van de band aan in millimeters.</p>
<p>Het tweede getal (<b><?php for($i = 0; $i < count($params['bandenmaatvoor']); $i++): ?><?=explode('/', $params['bandenmaatvoor'][$i])[1]?><?php if($i<count($params['bandenmaatvoor'])-1) echo ', '; ?><?endfor;?></b>) is de hoogte van de band.</p>
<p>Het derde getal met de letter R (<b><?php for($i = 0; $i < count($params['velgmaatinch']); $i++): ?>R<?=$params['velgmaatinch'][$i]?><?php if($i<count($params['velgmaatinch'])-1) echo ', '; ?><?endfor;?></b>) geeft de diameter aan voor de velgmaat. In dit geval is dat 16" (inch).</p>

<div class="page_break"></div>

<h4>Tax information</h4>
{{--<h1>TAX</h1>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 33%"/>
        <col style="width: 33%"/>
    </colgroup>
    <tr>
        <th>Province</th>
        <th>Tax per 3 months</th>
        <th>Tax per year</th>
    </tr>
    <?php for($i = 0; $i < count($params['provincie']); $i++): ?>
    <tr>
        <td><?=$params['provincie'][$i]?></td>
        <td><?=1 * $params['bedragKwartaal'][$i]?> &euro;</td>
        <td><?=4 * $params['bedragKwartaal'][$i]?> &euro;</td>
    </tr>
    <?php endfor; ?>
</table>


<h3>History APK</h3>
<h4>Known history</h4>
{{--<h1>HIST APK</h1>--}}
<?php for($i = 0; $i < count($params['volgnr']); $i++): ?>
<table class="formatted-table formatted-table-size">
    <tr>
        <td>volgnr</td>
        <td><?=$params['volgnr'][$i]?></td>
    </tr>
    <tr>
        <td>gebrek_identificatie</td>
        <td><?=$params['gebrek_identificatie'][$i]?></td>
    </tr>
    <tr>
        <td>gebrek_omschrijving</td>
        <td><?=$params['gebrek_omschrijving'][$i]?></td>
    </tr>
    <tr>
        <td>soort_actie</td>
        <td><?=$params['soort_actie'][$i]?></td>
    </tr>
    <tr>
        <td>aantal_gebreken</td>
        <td><?=$params['aantal_gebreken'][$i]?></td>
    </tr>
</table>
<br/>
<?php endfor; ?>

<div class="page_break"></div>

<h4>APK Rejects: Has this vehicle ever been rejected?</h4>
<!--todo-->
<h4>Last inspection</h4>
{{--<h1>APK afkeurpunten</h1>--}}
<p>Tijdens de algemene periodieke keuring (APK) wordt een voertuig gekeurd op verschillende punten. Zo kan het voertuig niet veilig genoeg zijn om op de openbare weg te mogen rijden. In dat geval wordt het voertuig afgekeurd en mag het niet op de weg.</p>
<p>Deze keuring is verplicht en mag alleen uitgevoerd worden door gecertificeerde keurmeesters. De keurmeester controleert het voertuig op: Banden, remmen, verlichting, uitstoot, wielophanging en nog veel meer punten.</p>

<div class="page_break"></div>

<h3>Owner history</h3>
<h4>When wast the car switched of owner?<h4>
{{--<h1>Eigenarenhistorie</h1>--}}
<?php for($i = 0; $i < count($params['datumAansprakelijkheidVanaf']); $i++): ?>
<h2>Eigenaar <?=$i + 1?></h2>
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Eigenaar vanaf</td>
        <td><?=$params['datumAansprakelijkheidVanaf'][$i]?></td>
    </tr>
    <tr>
        <td>Eigenaar tot</td>
        <td><?=isset($params['datumAansprakelijkheidTot'][$i]) ? $params['datumAansprakelijkheidTot'][$i] : 'heden'?></td>
    </tr>
    <tr>
        <td>Type eigenaar</td>
        <td><?=$params['typeEigenaar'][$i]?></td>
    </tr>
    <tr>
        <td>Aantal dagen in bezit</td>
        <td><?=$params['aantalDagen'][$i]?></td>
    </tr>
</table>
<br/>
<?php endfor; ?>

<div class="page_break"></div>

<h3>Advertisement history</h3>
<h4>Pricing history</h4>
{{--<h1>Advertentiegeschiedenis</h1>--}}
{{--<h2>Advertentie 1</h2>--}}
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: auto"/>
        <col style="width: 50%"/>
    </colgroup>
    <tr>
        <td>Website</td>
        <td></td>
    </tr>
    <tr>
        <td>Advertentiedatum</td>
        <td></td>
    </tr>
    <tr>
        <td>Foto's</td>
        <td></td>
    </tr>
    <tr>
        <td>Vraagprijs</td>
        <td></td>
    </tr>
</table>

<div class="page_break"></div>

<h4>Full known advertisement info</h4>
    {{--<h1>marktplaats</h1>--}}
    <table class="formatted-table formatted-table-size">
        <?php foreach($mp_data as $key => $value): ?>
        <tr>
            <td><?=$key?></td>
            <td><?=$value?></td>
        </tr>
        <?php endforeach; ?>
    </table>

<div class="page_break"></div>

<h4>Old pictures of the vehicle</h4>
        <!--todo-->
<h4>Owner history</h4>
        <!--todo-->

<h3>Damage History</h3>
<h4>WOK status</h4>
{{--<h1>Schadehistorie</h1>--}}
<p>WOK staat voor Wacht Op Keuren. Een auto met een WOK status wacht dus op keuring. Dit betekent in feite dat de auto op dat moment niet APK goedgekeurd is. Zonder APK mag er geen gebruik gemaakt worden van het voertuig.</p>
<p><b>Let op</b>: een WOK melding geeft niet aan dat een voertuig perse schade heeft gehad. Een voertuig kan ook een WOK status hebben als bijvoorbeeld de ramen te donker geblindeerd zijn of teveel geluid maakt etc.</p>
<p>Een WOK status kan verholpen worden door een APK keuring. De RDW Keurmeester controleert of het voertuig de weg op mag.</p>

<h2>Geen WOK status WOK status</h2>
<p>Resultaten voor kenteken <?=$carNumber?></p>

<div class="page_break"></div>

<h4>See what parts were damaged</h4>

{{--<h1>Schadehistorie</h1>--}}
<?php $i = 0; ?>
<?php foreach($damages['parameters'] as $key => $value): ?>
<?php if ($value == 'on'):?>
<?php $i++; ?>
<h2>Schade <?=$i?></h2>
<table class="formatted-table formatted-table-size">
    <colgroup>
        <col style="width: 25%"/>
        <col style="width: 25%"/>
        <col style="width: 25%"/>
        <col style="width: auto"/>
    </colgroup>
    <tr>
        <td>datum</td>
        <td>
            <?php if($damages['quarter'] == 1) echo 'Eerste Kwartaal'; ?>
            <?php if($damages['quarter'] == 2) echo 'Tweede kwartaal'; ?>
            <?php if($damages['quarter'] == 3) echo 'Derde Kwartaal'; ?>
            <?php if($damages['quarter'] == 4) echo 'Eerste Kwartaal'; ?>
        </td>
        <td rowspan="4">
            <img class="car-damage-picture" src="{{ url('/') . '/storage/images/Damage_Areas/' . $damagePhotos[$key] }}"/>
        </td>
        <td rowspan="4">
            <img class="car-damage-picture" src="{{ url('/') . '/storage/images/Damage_Areas/' . $damagePhotos['top_'.$key] }}"/>
        </td>
    </tr>
    <tr>
        <td>schadebedrag</td>
        <td><?=$damages['price']?></td>
    </tr>
    <tr>
        <td>km-stand</td>
        <td><?=$damages['mileage']?></td>
    </tr>
    <tr>
        <td>schadepositie</td>
        <td><?=$key?></td>
    </tr>
</table>
<?php endif; ?>
<?php endforeach; ?>

<div class="page_break"></div>
    <table>
    @foreach($damages['images'] as $image)
        <tr><td>
            <img class="car-damage-image" src="http://85.214.132.37:8000/schadeautos/cropped_images_sa/{{$image}}"/>
        </td></tr>
    @endforeach
    </table>
<div class="page_break"></div>

<h4>Pull back actions</h4>
    <!--todo-->

<h3>Price estimation</h3>
<h4>Tax estimation</h4>
{{--<h1>TAX ONLINE</h1>--}}
<table class="formatted-table formatted-table-size">
    <tr>
        <td>DagwaardeVerkoop</td>
        <td><?=$params['dagwaardeVerkoop']?></td>
    </tr>
    <tr>
        <td>DagwaardeInruil</td>
        <td><?=$params['dagwaardeInruil']?></td>
    </tr>
    <tr>
        <td>DagwaardeHandel</td>
        <td><?=$params['dagwaardeHandel']?></td>
    </tr>
</table>


<h4>Price estimation</h4>
<!--todo-->










<style>
    .energy-label-picture {
        width: 200px;
    }

    .car-damage-picture {
        max-width: 150px;
    }

    .car-damage-image {
        max-width: 600px;
    }

    .formatted-table-size {
        width: 100%;
        table-layout: fixed;
    }

    .formatted-table
    {
        margin: 0 0;
        border-collapse: collapse;
        text-align: left;
    }
    .formatted-table th
    {
        font-size: 14px;
        font-weight: bold;
        padding: 4px 0;
        border-bottom: 1px solid #ccc;
    }
    .formatted-table td
    {
        font-size: 14px;
        border-bottom: 1px solid #ccc;
        padding: 4px 0;
    }

    .formatted-table tr:nth-child(even) {
        background-color: #f7f7f7
    }

    h1 {
        font-size: 14pt;
        border-bottom: 3px solid #0396c5;
        margin-top: 20px;
    }
    h2 {
        font-size: 14px;
    }

    .image-logo {
        width: 250px;
    }

    .page_break {
        page-break-before: always;
    }

    .bandenmaten-table {
        width: 50%;
        float: left;
    }

    .bandenmaten-pic {
        max-height:80px;
    }

    .plate-number-container {
        position: relative;
        text-align: center;
    }

    .plate-number-text {
        position: absolute;
        top: 14px;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 50px;
    }

    .reportdatum {
        text-align: center;
    }
</style>