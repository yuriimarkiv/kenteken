@extends('adminlte::page')

@section('title', 'Goods')

@section('content_header')
    <h1>Goods</h1>
@stop

@section('content')
    <div class="box box-primary">
        @if ($errors->any())
            <ul class="errors">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="box-header with-border">
            <h3 class="box-title">{{isset($good->id) ? 'Update' : 'Create'}}</h3>
        </div>
        <form action="{{isset($good->id) ? route('goods.update', $good->id) : route('goods.index')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field(isset($good->id) ? 'PUT' : 'POST') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Title</label>
                    <input class="form-control" id="title" name="title" value="{{ old('title', $good->title) }}"/>
                </div>
                <div class="form-group">
                    <label for="name">Description</label>
                    <input class="form-control" id="description" name="description" value="{{ old('description', $good->description) }}"/>
                </div>
                <div class="form-group">
                    <label for="name">Price</label>
                    <input class="form-control" id="price" name="price" value="{{ old('price', $good->price) }}"/>
                </div>
                <div class="form-group">
                    <label for="image">Attach image</label>
                    <input type="file" id="image" name="image">
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
                <a href="{{url('/goods')}}" class="btn btn-default">Cancel</a>
            </div>
        </form>
    </div>

@stop