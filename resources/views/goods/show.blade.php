@extends('adminlte::page')

@section('title', 'Goods')

@section('content_header')
    <h1>Goods</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{$good->title}}</h3>
            <a href="{{ url('goods/' . $good->id . '/edit') }}" class=""><i class="fa fa-edit mr-2" aria-hidden="true"></i></a>
            <div class="box-body">
                <p>Description: {{$good->description}}</p>
                <p>Price: {{$good->price}}</p>
                @if($good->photo)
                    <img class="attached-photo" src="{{ url('/') }}{{ $good->photo ? Storage::url("$good->photo") : ''}}" alt="picture">
                @endif
            </div>
        </div>
    </div>
@stop