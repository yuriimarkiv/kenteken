@extends('adminlte::page')

@section('title', 'Goods')

@section('content_header')
    <h1>Goods</h1>
@stop

@section('content')
    <section class="content">
        <a href="{{'goods/create'}}" class="btn btn-primary" >Create</a>
        <br/><br/>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Goods</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover table-striped dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Photo</th>
                                            <th>Price</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--<tr>--}}
                                            {{--<form class="form-inline search" id="search" method="get" action="/{{ Request::path() }}">--}}
                                                {{--<td>--}}
                                                    {{--<input type="text" class="form-control" name="searchName" autocomplete="off" placeholder="Name" value="{{Request::input('searchName')}}">--}}
                                                {{--</td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<input type="submit" hidden>--}}
                                            {{--</form>--}}
                                        {{--</tr>--}}
                                        @foreach($goods as $good)
                                            <tr role="row" class="odd">
                                                <td><a href="{{ url('goods/' . $good->id) }}">{{$good->title}}</a></td>
                                                <td>{{$good->description}}</td>
                                                <td><img src="{{ url('/') }}{{ $good->photo ? Storage::url("$good->photo") : ''}}" alt="picture" class="goods-attached-photo"></td>
                                                <td>{{$good->price}}</td>
                                                <td width="240">
                                                    <a href="{{ url('goods/' . $good->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit mr-2" aria-hidden="true"></i>View</a>
                                                    <a href="{{ url('goods/' . $good->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-edit mr-2" aria-hidden="true"></i>Update</a>
                                                    <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#delete_{{$good->id}}"><i class="fa fa-trash mr-2" aria-hidden="true"></i>Delete</button>

                                                    <div class="modal fade" id="delete_{{$good->id}}" tabindex="-1" role="dialog" aria-labelledby="modal_{{$good->id}}" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="modal_{{$good->id}}">Confirm</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Are you sure you want to delete this item?</p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                    <form action='{{ route('goods.destroy', $good->id) }}' method="POST" style="display: inline">
                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        <button type="submit" class="btn btn-danger" style="margin-right: 4px">Confirm</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{$goods
                                        /*->appends([
                                            'searchName' => Request::get('searchName'),
                                            'searchEmail' => Request::get('searchEmail'),
                                        ])*/
                                        ->links()}}
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>

@stop