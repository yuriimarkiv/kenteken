@extends('common-tamplate')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
                <form class="kenteken-form" action="{{url('/kenteken/show-short-pdf')}}" method="post">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <div class="form-group">
                        <label for="kenteken-input">Kenteken</label>
                        <input type="text" class="form-control kenteken-input" id="kenteken-input" name="kentekenInput" aria-describedby="kenteken-input-help" placeholder="">
                        <small id="kenteken-input-help" class="form-text text-muted">vul hier het kenteken in:</small>
                    </div>
                    <button type="submit" class="btn btn-primary kenteken-submit-btn">controleer</button>
                </form>
        </div>
    </div>
@stop