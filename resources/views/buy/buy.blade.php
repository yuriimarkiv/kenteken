@extends('common-tamplate')

@section('content')
<h2>Buy</h2>
<div class="row">
    <div class="col-md-4 offset-md-4">
        @foreach($goods as $item)
        <div class="buy-goods-container">
            <h3>{{$item->title}}</h3>
            <div class="buy-goods-content">
                <div class="buy-goods-left">
                    <img class="buy-goods-photo" src="{{ url('/') }}{{  Storage::url("images/logo.png") }}" alt="no picture">
                </div>
                <div class="buy-goods-right">{{$item->description}}</div>
            </div>

            <div class="buy-goods-bottom">
                <div class="buy-goods-bottom-left">
                    Price: {{$item->price}}$
                </div>
                <div class="buy-goods-bottom-right">
                    <a href="{{url("/buy/add-to-cart/$item->id")}}" class="btn btn-success">
                        Add to card
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@stop