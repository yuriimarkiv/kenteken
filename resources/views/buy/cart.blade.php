@extends('common-tamplate')

@section('content')

<h2>Cart</h2>
<?php
var_dump($goods);
?>
<div class="row">
    <div class="col-md-4 offset-md-4">
        <form action="{{isset($order->id) ? route('buy.confirm', $order->id) : url('/buy/confirm')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field(isset($good->id) ? 'PUT' : 'POST') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" id="name" name="name" value="{{ old('name', $order->name) }}"/>
                </div>
                <div class="form-group">
                    <label for="name">Address</label>
                    <input class="form-control" id="address" name="address" value="{{ old('address', $order->address) }}"/>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
                <a href="{{url('/buy')}}" class="btn btn-default">Cancel</a>
            </div>
        </form>
    </div>
</div>

@stop