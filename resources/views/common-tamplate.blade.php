<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Kenteken Verslag</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body class="home1">

<div class="row">
    <div class="col-md-6">
        <a href="/"><img src="{{ url('/')."/storage/images/logo.png" }}" class="image-logo-home" alt="site logo" /></a>
        {{--<h1>Kenteken Verslag</h1>--}}
    </div>
    <div class="col-md-6">
        <div class="home-login-btn">
            {{--<a href="/buy/cart">cart</a>--}}
            {{--<a href="/buy">buy</a>--}}
            @if(!auth()->guest())
                <a href="/home">admin</a>
                <a href="/kenteken/cabinet">cabinet</a>
            @endif
            @if(auth()->guest())
                <a href="/login">login</a>
            @else
                <a href="#"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                >
                    <i class="fa fa-fw fa-power-off"></i> logout
                </a>
                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                    @if(config('adminlte.logout_method'))
                        {{ method_field(config('adminlte.logout_method')) }}
                    @endif
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
    </div>
</div>

@yield('content')

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>