@extends('adminlte::page')

@section('title', 'Statistics')

@section('content_header')
    <h1>Statistics</h1>
@stop

@section('content')
    <section class="content">
        <ul>
            <li>Total number of clients in DB: <?=$numberClients?></li>
            <li>Total # of people that did a free check but never bought: <?=$numberFreeCheck?></li>
            <li>Total # of clients that have bought: <?=$numberBought?></li>
            <li>At what time we have the most clients:
                <ul>
                @foreach($clientsTimeline as $t => $c)
                    <li>
                        {{$t}} {{$c}}
                    </li>
                @endforeach
                </ul>
            </li>
<br/>
            <li>Total number of clients in DB per month: <?=$numberClientsMonth?></li>
            <li>Total # of people that did a free check but never bought per month: <?=$numberFreeCheckMonth?></li>
            <li>Total # of clients that have bought per month: <?=$numberBoughtMonth?></li>
            <li>At what time we have the most clients per month:
                <ul>
                    @foreach($clientsTimelineMonth as $t => $c)
                        <li>
                            {{$t}} {{$c}}
                        </li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </section>
@stop