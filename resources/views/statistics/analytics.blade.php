@extends('adminlte::page')

@section('title', 'Statistics')

@section('content_header')
    <h1>Analytics stats</h1>
@stop

@section('content')
    <section class="content">
        <ul>
            <li>1. unique visitors: <?=$uniqueVisitors?></li>
            <li>2. Referrals: <?=$referrals?></li>
            <li>2B. Top 20 Keywords and Key Phrases that were used:
                @foreach($topKeywords as $word)
                    <?=$word?>
                @endforeach
            </li>
            <li>3. Bounce (a few seconds on page. does not try any functions): <?=$bounce?></li>
            <li>4. Exit   (Tries a lookup , doesnt like it , and leaves the webstie): <?=$leaves?></li>
            <li>5. Top 10 Pages tha are most used (ranked in order): <?=$topPages?></li>
        </ul>
    </section>
@stop