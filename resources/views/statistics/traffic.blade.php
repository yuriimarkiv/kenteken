@extends('adminlte::page')

@section('title', 'Statistics')

@section('content_header')
    <h1>Traffic</h1>
@stop

@section('content')
    <section class="content">
        <ul>
            <h3>Traffic :</h3>
            <li>Organic Search: traffic coming via the search engines: <?=$organicSearch?></li>
            <li>Referral: traffic from another website (comes later): <?=$referral?></li>
            <li>Direct: traffic typing your domain into the browser: <?=$direct?></li>
            <li>Social: traffic from social media: <?=$social?></li>
            <h3>Conversion rate</h3>
            <li>Organic search traffic: % conversion rate: <?=$conversionOrganicSearch?></li>
            <li>Referral traffic: % conversion rate: <?=$conversionReferral?></li>
            <li>Direct traffic: % conversion rate: <?=$conversionDirect?></li>
            <li>Social traffic: % conversion rate: <?=$conversionSocial?></li>
        </ul>
    </section>
@stop