@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <h1>Users</h1>
@stop

@section('content')
    <div class="box box-primary">
        @if ($errors->any())
            <ul class="errors">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="box-header with-border">
            <h3 class="box-title">{{isset($user->id) ? 'Update' : 'Create'}}</h3>
        </div>
        <form action="{{isset($user->id) ? route('users.update', $user->id) : route('users.index')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field(isset($user->id) ? 'PUT' : 'POST') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" id="name" name="name" value="{{ old('name', $user->name) }}"/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="email" id="email" name="email" value="{{ old('email', $user->email) }}"/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" id="password" name="password" />
                </div>
                <div class="form-group">
                    @foreach($roles as $role)
                        <input type="checkbox"
                               name="roles[]"
                               value="{{$role->id}}"
                                {{ in_array($role->id, $checkedRoles) ? 'checked': '' }}> <label>{{$role->name}}</label>
                    @endforeach
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{url('/users')}}" class="btn btn-default">Cancel</a>
            </div>
        </form>
    </div>

@stop