@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <h1>Users</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{$user->name}}</h3>
            <a href="{{ url('users/' . $user->id . '/edit') }}" class=""><i class="fa fa-edit mr-2" aria-hidden="true"></i></a>
            <div class="box-body">
                <p>Email: {{$user->email}}</p>
                <p>Roles: {{$user->roles()->pluck('name')->implode(', ')}}</p>
            </div>
        </div>
    </div>
@stop