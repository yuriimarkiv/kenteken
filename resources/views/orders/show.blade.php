@extends('adminlte::page')

@section('title', 'Orders')

@section('content_header')
    <h1>Orders</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{$order->name}}</h3>
            <a href="{{ url('orders/' . $order->id . '/edit') }}" class=""><i class="fa fa-edit mr-2" aria-hidden="true"></i></a>
            <div class="box-body">
                <p>Description: {{$order->address}}</p>
                <p>Status: {{$order->status}}</p>

            </div>
        </div>
    </div>
@stop