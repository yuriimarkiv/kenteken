@extends('adminlte::page')

@section('title', 'Orders')

@section('content_header')
    <h1>Orders</h1>
@stop

@section('content')
    <div class="box box-primary">
        @if ($errors->any())
            <ul class="errors">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <div class="box-header with-border">
            <h3 class="box-title">{{isset($order->id) ? 'Update' : 'Create'}}</h3>
        </div>
        <form action="{{isset($order->id) ? route('orders.update', $order->id) : route('orders.index')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field(isset($order->id) ? 'PUT' : 'POST') }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" id="name" name="name" value="{{ old('name', $order->name) }}"/>
                </div>
                <div class="form-group">
                    <label for="name">Address</label>
                    <input class="form-control" id="address" name="address" value="{{ old('address', $order->address) }}"/>
                </div>
                <div class="form-group">
                    <label for="name">Status</label>
                    <input class="form-control" id="status" name="status" value="{{ old('status', $order->status) }}"/>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
                <a href="{{url('/orders')}}" class="btn btn-default">Cancel</a>
            </div>
        </form>
    </div>

@stop